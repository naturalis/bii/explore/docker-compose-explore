--
--- Create aggregation table

\c db1

SET h3.extend_antimeridian TO true;

CREATE EXTENSION IF NOT EXISTS topn;
CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS h3;
CREATE EXTENSION IF NOT EXISTS hll;

CREATE TABLE gbif_aggregated_nl AS
SELECT 
	--DATE_TRUNC('month', eventdate) AS eventdate,
	--make_date(year::int,month::int,1) AS eventdate,
	countrycode,
	--family,
	year,
	--month,
	h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 2) AS h3_2,
	--topn_add_agg((DATE_TRUNC('month', eventdate))::text) AS topn_eventdate,
	topn_add_agg(countrycode) AS topn_countrycode,
	topn_add_agg(family) AS topn_family,
	topn_add_agg(year::text) AS topn_year,
	topn_add_agg(month::text) AS topn_month,
	topn_add_agg((h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 2))::text) AS topn_h3_2,
	topn_add_agg(scientificname) AS topn_scientificname,
	topn_add_agg(recordedby) AS topn_recordedby,
	hll_add_agg(hll_hash_bigint(gbifid::bigint)) AS hll_gbifid, -- Count using hll
	count(*) AS just_count, -- Just count
	--hll_add_agg(hll_hash_text((DATE_TRUNC('month', eventdate))::text)) AS hll_eventdate,
	hll_add_agg(hll_hash_text(countrycode)) AS hll_countrycode,
	hll_add_agg(hll_hash_text(family)) AS hll_family,
	hll_add_agg(hll_hash_integer(year::int)) AS hll_year,
	hll_add_agg(hll_hash_integer(month::int)) AS hll_month,
	hll_add_agg(hll_hash_text(h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 2)::text)) AS hll_h3_2,
	hll_add_agg(hll_hash_text(scientificname)) AS hll_scientificname,
	hll_add_agg(hll_hash_text(recordedby)) AS hll_recordedby
FROM gbif_psql_raw_nl
GROUP BY 1,2,3;


--CREATE INDEX ON gbif_aggregated USING btree (eventdate);
CREATE INDEX ON gbif_aggregated USING brin (year);
CREATE INDEX ON gbif_aggregated USING gin (topn_family);
CREATE INDEX ON gbif_aggregated USING gin (topn_countrycode);
CREATE INDEX ON gbif_aggregated USING gin (topn_scientificname);
CREATE INDEX ON gbif_aggregated USING gin (topn_recordedby);
--
