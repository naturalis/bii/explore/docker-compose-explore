--
\c db1

CREATE EXTENSION IF NOT EXISTS h3;
CREATE EXTENSION IF NOT EXISTS hll;
CREATE EXTENSION IF NOT EXISTS postgis;


-- Table scientificname
CREATE TABLE IF NOT EXISTS public.scientificname
(
    scientificname_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    scientificname text COLLATE pg_catalog."default",
    lastupdate timestamp with time zone,
    tsv tsvector GENERATED ALWAYS AS (to_tsvector('simple', scientificname)) STORED,
    CONSTRAINT scientificname_pkey PRIMARY KEY (scientificname_id),
    CONSTRAINT scientificname_scientificname_key UNIQUE (scientificname)
);

CREATE INDEX ON public.scientificname USING BRIN (scientificname_id);
CREATE INDEX ON public.scientificname USING BTREE (scientificname ASC NULLS LAST);
CREATE INDEX ON public.scientificname USING GIN (tsv);

MERGE INTO scientificname s
USING (SELECT DISTINCT scientificname FROM gbif_psql_raw) AS r
ON (s.scientificname = r.scientificname)
WHEN MATCHED THEN DO NOTHING
WHEN NOT MATCHED THEN
INSERT (scientificname)
VALUES (r.scientificname);


-- Table locality
CREATE TABLE IF NOT EXISTS public.locality
(
    locality_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    locality text COLLATE pg_catalog."default",
    lastupdate timestamp with time zone,
    tsv tsvector GENERATED ALWAYS AS (to_tsvector('simple', locality)) STORED,
    CONSTRAINT locality_pkey PRIMARY KEY (locality_id),
    CONSTRAINT locality_locality_key UNIQUE (locality)
);

CREATE INDEX ON public.locality USING BRIN (locality_id);
CREATE INDEX ON public.locality USING BTREE (locality ASC NULLS LAST);
CREATE INDEX ON public.locality USING GIN (tsv);

MERGE INTO locality l
USING (SELECT DISTINCT locality FROM gbif_psql_raw) AS r
ON (l.locality = r.locality)
WHEN MATCHED THEN DO NOTHING
WHEN NOT MATCHED THEN
INSERT (locality)
VALUES (r.locality);


-- Table countrycode
CREATE TABLE IF NOT EXISTS public.countrycode
(
    countrycode_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    countrycode text COLLATE pg_catalog."default",
    lastupdate timestamp with time zone,
    tsv tsvector GENERATED ALWAYS AS (to_tsvector('simple', countrycode)) STORED,
    CONSTRAINT countrycode_pkey PRIMARY KEY (countrycode_id),
    CONSTRAINT countrycode_countrycode_key UNIQUE (countrycode)
);

CREATE INDEX ON public.countrycode USING BRIN (countrycode_id);
CREATE INDEX ON public.countrycode USING BTREE (countrycode ASC NULLS LAST);
CREATE INDEX ON public.countrycode USING GIN (tsv);

MERGE INTO countrycode l
USING (SELECT DISTINCT countrycode FROM gbif_psql_raw) AS r
ON (l.countrycode = r.countrycode)
WHEN MATCHED THEN DO NOTHING
WHEN NOT MATCHED THEN
INSERT (countrycode)
VALUES (r.countrycode);


-- Table recordedby
CREATE TABLE IF NOT EXISTS public.recordedby
(
    recordedby_id integer NOT NULL GENERATED ALWAYS AS IDENTITY ( INCREMENT 1 START 1 MINVALUE 1 MAXVALUE 2147483647 CACHE 1 ),
    recordedby text COLLATE pg_catalog."default",
    lastupdate timestamp with time zone,
    tsv tsvector GENERATED ALWAYS AS (to_tsvector('simple', recordedby)) STORED,
    CONSTRAINT recordedby_pkey PRIMARY KEY (recordedby_id),
    CONSTRAINT recordedby_recordedby_key UNIQUE (recordedby)
);

CREATE INDEX ON public.recordedby USING BRIN (recordedby_id);
CREATE INDEX ON public.recordedby USING BTREE (recordedby ASC NULLS LAST);
CREATE INDEX ON public.recordedby USING GIN (tsv);

MERGE INTO recordedby l
USING (SELECT DISTINCT recordedby FROM gbif_psql_raw) AS r
ON (l.recordedby = r.recordedby)
WHEN MATCHED THEN DO NOTHING
WHEN NOT MATCHED THEN
INSERT (recordedby)
VALUES (r.recordedby);


-- Create table gbif_enriched
CREATE UNLOGGED TABLE IF NOT EXISTS public.gbif_enriched
(
    gbif_enriched_id bigint NOT NULL GENERATED ALWAYS AS IDENTITY,
    gbifid numeric NOT NULL,
    datasetkey text COLLATE pg_catalog."default",
    occurrenceid text COLLATE pg_catalog."default",
    kingdom text COLLATE pg_catalog."default",
    phylum text COLLATE pg_catalog."default",
    class text COLLATE pg_catalog."default",
    "order" text COLLATE pg_catalog."default",
    family text COLLATE pg_catalog."default",
    genus text COLLATE pg_catalog."default",
    species text COLLATE pg_catalog."default",
    infraspecificepithet text COLLATE pg_catalog."default",
    taxonrank text COLLATE pg_catalog."default",
    scientificname_id integer,
    verbatimscientificname text COLLATE pg_catalog."default",
    verbatimscientificnameauthorship text COLLATE pg_catalog."default",
    countrycode_id integer,
    locality_id integer,
    stateprovince text COLLATE pg_catalog."default",
    occurrencestatus text COLLATE pg_catalog."default",
    individualcount numeric,
    publishingorgkey text COLLATE pg_catalog."default",
    decimallatitude numeric,
    decimallongitude numeric,
    coordinateuncertaintyinmeters numeric,
    coordinateprecision numeric,
    elevation numeric,
    elevationaccuracy numeric,
    depth numeric,
    depthaccuracy numeric,
    eventdate timestamp without time zone,
    day int,
    month int,
    year int,
    taxonkey numeric,
    specieskey numeric,
    basisofrecord text COLLATE pg_catalog."default",
    institutioncode text COLLATE pg_catalog."default",
    collectioncode text COLLATE pg_catalog."default",
    catalognumber text COLLATE pg_catalog."default",
    recordnumber text COLLATE pg_catalog."default",
    identifiedby text COLLATE pg_catalog."default",
    dateidentified timestamp without time zone,
    license text COLLATE pg_catalog."default",
    rightsholder text COLLATE pg_catalog."default",
    recordedby_id integer,
    typestatus text COLLATE pg_catalog."default",
    establishmentmeans text COLLATE pg_catalog."default",
    lastinterpreted timestamp without time zone,
    mediatype text COLLATE pg_catalog."default",
    issue text COLLATE pg_catalog."default",
    h3_1 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 1)) STORED,
    h3_3 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 3)) STORED,
    h3_5 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 5)) STORED,
    h3_7 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 7)) STORED,
    h3_9 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 9)) STORED,
    h3_11 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 11)) STORED,
    h3_13 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 13)) STORED,
    h3_15 h3index GENERATED ALWAYS AS (h3_lat_lng_to_cell(point((decimallongitude)::double precision, (decimallatitude)::double precision), 15)) STORED,
	scientificname_hll hll_hashval GENERATED ALWAYS AS (hll_hash_integer(scientificname_id)) STORED,
    CONSTRAINT gbif_pkey PRIMARY KEY (gbifid),
    CONSTRAINT fk_gbif_enriched_locality FOREIGN KEY (locality_id)
        REFERENCES public.locality (locality_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_gbif_enriched_scientificname FOREIGN KEY (scientificname_id)
        REFERENCES public.scientificname (scientificname_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_gbif_enriched_countrycode FOREIGN KEY (countrycode_id)
        REFERENCES public.countrycode (countrycode_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION,
    CONSTRAINT fk_gbif_enriched_recordedby FOREIGN KEY (recordedby_id)
        REFERENCES public.recordedby (recordedby_id) MATCH SIMPLE
        ON UPDATE NO ACTION
        ON DELETE NO ACTION	
);

CREATE INDEX ON public.gbif_enriched USING btree (gbifid);
CREATE INDEX ON public.gbif_enriched USING btree (eventdate);
CREATE INDEX ON public.gbif_enriched USING btree (year);
CREATE INDEX ON public.gbif_enriched USING btree (month);
CREATE INDEX ON public.gbif_enriched USING btree (locality_id);
CREATE INDEX ON public.gbif_enriched USING btree (scientificname_id);
CREATE INDEX ON public.gbif_enriched USING btree (recordedby_id);


-- -- Insert gbif_psql_raw -> gbif_enriched using merge (1h34m)
-- -- ERROR:  UPDATE and CTID scans not supported for ColumnarScan
-- WITH a AS (
-- SELECT r.gbifid, r.eventdate, r.year, r.month, c.countrycode_id, l.locality_id, r.decimallatitude, r.decimallongitude, s.scientificname_id, rb.recordedby_id FROM public.gbif_psql_raw r
-- LEFT JOIN scientificname s ON r.scientificname = s.scientificname
-- LEFT JOIN locality l ON r.locality = l.locality
-- LEFT JOIN countrycode c ON r.countrycode = c.countrycode
-- LEFT JOIN recordedby rb ON r.recordedby = rb.recordedby
-- )
-- MERGE INTO public.gbif_enriched e
-- USING a
-- ON a.gbifid = e.gbifid
-- WHEN MATCHED THEN DO NOTHING
-- --WHEN MATCHED AND a.scientificname_id <> e.scientificname_id THEN 
-- --UPDATE SET scientificname_id = a.scientificname_id, locality_id = a.locality_id
-- WHEN NOT MATCHED THEN
-- INSERT (gbifid, eventdate, year, month, countrycode_id, locality_id, decimallatitude, decimallongitude, scientificname_id, recordedby_id)
-- VALUES (a.gbifid, a.eventdate, a.year, a.month, a.countrycode_id, a.locality_id, a.decimallatitude, a.decimallongitude, a.scientificname_id, a.recordedby_id);


-- Insert not using merge
INSERT INTO public.gbif_enriched (gbifid, eventdate, year, month, countrycode_id, locality_id, decimallatitude, decimallongitude, scientificname_id, recordedby_id)
SELECT r.gbifid, r.eventdate, r.year, r.month, c.countrycode_id, l.locality_id, r.decimallatitude, r.decimallongitude, s.scientificname_id, rb.recordedby_id FROM public.gbif_psql_raw r
LEFT JOIN scientificname s ON r.scientificname = s.scientificname
LEFT JOIN locality l ON r.locality = l.locality
LEFT JOIN countrycode c ON r.countrycode = c.countrycode
LEFT JOIN recordedby rb ON r.recordedby = rb.recordedby;


-- -- Insert locality_id into gbif_enriched
-- WITH a AS (
-- SELECT r.gbifid, l.locality_id FROM public.gbif_psql_raw r
-- LEFT JOIN locality l
-- ON r.locality = l.locality
-- )
-- MERGE INTO public.gbif_enriched e
-- USING a
-- ON MD5(ROW(a.gbifid, a.locality_id)::text) = MD5(ROW(e.gbifid, e.locality_id)::text)
-- WHEN MATCHED THEN DO NOTHING
-- WHEN NOT MATCHED THEN
-- INSERT (gbifid, locality_id)
-- VALUES (a.gbifid, a.locality_id);


-- -- Add timestamp on change
-- CREATE OR REPLACE FUNCTION update_modified_column()   
-- RETURNS TRIGGER AS $$
-- BEGIN
--     NEW.modified = now();
--     RETURN NEW;   
-- END;
-- $$ language 'plpgsql';

-- CREATE TRIGGER update_modified_column BEFORE UPDATE ON scientificname FOR EACH ROW EXECUTE PROCEDURE update_modified_column();
