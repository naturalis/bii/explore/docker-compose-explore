#/bin/bash
set -e

wget -P /var/lib/postgresql/data https://api.gbif.org/v1/occurrence/download/request/0107080-200613084148143.zip
unzip /var/lib/postgresql/data/0107080-200613084148143.zip -d /var/lib/postgresql/data

# Remove headerline from occurence.txt
tail -n +2 /var/lib/postgresql/data/occurrence.txt > /var/lib/postgresql/data/gbif_raw.tsv

# Remove headerline from multimedia.txt
tail -n +2 /var/lib/postgresql/data/multimedia.txt > /var/lib/postgresql/data/multimedia_raw.tsv
