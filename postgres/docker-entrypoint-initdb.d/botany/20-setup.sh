#/bin/bash
set -e

wget -P /var/lib/postgresql/data https://api.gbif.org/v1/occurrence/download/request/0266669-200613084148143.zip
unzip /var/lib/postgresql/data/0266669-200613084148143.zip -d /var/lib/postgresql/data

# Remove headerline from occurence.txt
tail -n +2 /var/lib/postgresql/data/occurrence.txt > /var/lib/postgresql/data/gbif_raw.tsv

# Remove rows with incorrect values
sed -i -e 215166d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 955221d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 1449592d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 1578092d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 2041117d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 2090923d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 2274063d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 2948274d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 2964065d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 3216290d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 3406478d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 3852303d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 4216860d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 4223270d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 4361782d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 4459671d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 4476781d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 4517094d /var/lib/postgresql/data/gbif_raw.tsv
sed -i -e 4778815d /var/lib/postgresql/data/gbif_raw.tsv

# Remove headerline from multimedia.txt
tail -n +2 /var/lib/postgresql/data/multimedia.txt > /var/lib/postgresql/data/multimedia_raw.tsv
