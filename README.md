# Explore GBIF datasets

### Description
Grafana dashboard with interactive map to explore a GBIF datasets. Clickhouse and PostgreSQL backend datastores.

### How to use

#### Requirements
Tested with:
- Docker compose v2
- Ubuntu 22.04


#### Start
Clone this repo:
```
git clone git@gitlab.com:naturalis/bii/explore/docker-compose-explore.git
```

Start the containers:
```
docker compose up -d
```

Browse to http://localhost:8050 in your browser to use the dashboard.
