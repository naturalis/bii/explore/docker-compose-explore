#!/bin/bash
set -euo pipefail

echo -e "Start containers..."
docker compose up -d clickhouse postgres explore

sleep 5

echo -e "Setup tables Postgres..."
docker compose exec postgres bash -c 'psql -v ON_ERROR_STOP=1 -U postgres < /var/lib/postgresql/scripts/10-gbif.sql'

echo -e "Setup tables Clickhouse..."
docker compose exec clickhouse bash '/var/lib/clickhouse/user_files/setup_sample.sh'

echo -e "Setup aggregate tables Postgres"
docker compose exec postgres bash -c 'psql -v ON_ERROR_STOP=1 -U postgres < /var/lib/postgresql/scripts/30-gbif.sql'

