WITH raw AS
    (
        WITH optimal_resolution AS (
                WITH
                    a AS
                    (
                        --SELECT [COLUMNS('h3') APPLY uniqCombined(12)] AS my_array
                        SELECT [COLUMNS('h3') APPLY uniq] AS my_array
                        FROM gbif_enriched_nl
                        WHERE latitude BETWEEN %s AND %s AND longitude BETWEEN %s AND %s
			AND year BETWEEN %s AND %s
			{% if classification %}
                        AND hasAny(array(class), {{ classification }})
                        {% endif %}
			{% if collection %}    
                        AND hasAny(array(collectioncode), {{ collection }})         
                        {% endif %}
                        {% if recordedby %}
                        AND match(lower(recordedby_string), lower('{{ recordedby }}'))
                        {% endif %}
                    ),
                    b AS
                    (
                        SELECT arrayMap(x -> abs(x - 842), my_array) AS diff
                        FROM a
                    ),
                    c AS
                    (
                        SELECT range(1, 6) AS range_index
                    ),
                    d AS
                    (
                        SELECT
                            my_array,
                            diff,
                            range_index
                        FROM a, b, c
                    )
                SELECT range_index
                FROM d
                ARRAY JOIN
                    my_array,
                    diff,
                    range_index
                ORDER BY
                    diff ASC,
                    range_index DESC
                LIMIT 1
            )
        SELECT
            count(*) AS cnt,
            range_index,
            [h3_1, h3_3, h3_5, h3_7, h3_9][range_index] AS h3_index
        FROM gbif_enriched_nl, optimal_resolution
        WHERE latitude BETWEEN %s AND %s AND longitude BETWEEN %s AND %s
	AND year BETWEEN %s AND %s
        {% if classification %}
        AND hasAny(array(class), {{ classification }})
        {% endif %}
	{% if collection %}    
        AND hasAny(array(collectioncode), {{ collection }})         
        {% endif %}
        {% if recordedby %}
        AND match(lower(recordedby_string), lower('{{ recordedby }}'))
        {% endif %}
        GROUP BY
            3,
            2
    )
  SELECT
      cnt,
      range_index,
      h3ToGeo(assumeNotNull(h3_index)).2 AS latitude,
      h3ToGeo(assumeNotNull(h3_index)).1 AS longitude,
      h3_index,
      normalize_polygon(h3ToGeoBoundary(assumeNotNull(h3_index))) AS boundary
FROM raw

