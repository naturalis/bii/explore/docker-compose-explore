import dash
import dash_mantine_components as dmc
from dash_iconify import DashIconify
from dash import (
    Dash,
    _dash_renderer,
    html,
    dcc,
    callback,
    Input,
    Output,
    State,
    ctx,
    register_page,
)
import os
import dash_leaflet as dl
from dash import dash_table
from dash.dependencies import Input, Output
import json
import logging
import pandas as pd
import clickhouse_connect
from clickhouse_connect import common
from clickhouse_connect.driver import httputil
import datetime as dt
from jinja2 import Template
import plotly.express as px
import numpy
import h3
import plotly.graph_objects as go
import random


register_page(__name__, path="/")


# Configure logging to print to console
logging.basicConfig(level=logging.INFO)

# ClickHouse configurations
clickhouse_host = os.getenv("CLICKHOUSE_HOST")
clickhouse_user = os.getenv("CLICKHOUSE_USER")
clickhouse_pass = os.getenv("CLICKHOUSE_PASSWORD")

# Clickhouse multi query
common.set_setting("autogenerate_session_id", False)

# Clickhouse pool manager
big_pool_mgr = httputil.get_pool_manager(maxsize=16, num_pools=12)

client = clickhouse_connect.get_client(
    pool_mgr=big_pool_mgr,
    host=clickhouse_host,
    port=8123,
    user=clickhouse_user,
    password=clickhouse_pass,
    database="db1",
)

## Function to convert integer H3 index to string and get the geographical boundary
# def get_geo_boundary(h3_index):
#    h3_index_str = h3.h3_to_string(h3_index)  # Convert integer to H3 string
#    return h3.h3_to_geo_boundary(h3_index_str, geo_json=True)


## Function to scale count from 1-1000 to 1-20
# def scale_opacity(count):
#    return 1 + (count - 1) * (20 - 1) / (1000 - 1)


## Function to convert integer H3 index to string and get the GeoJSON feature
# def get_geojson_feature(h3_index):
#    h3_index_str = h3.h3_to_string(h3_index)  # Convert integer to H3 string
#    boundary = h3.h3_to_geo_boundary(h3_index_str, geo_json=True)
#    # opacity = scale_opacity(count)
#    feature = {
#        "type": "Feature",
#        "geometry": {"type": "Polygon", "coordinates": [boundary]},
#        "properties": {
#            "tooltip": h3_index_str,
#            "popup": h3_index_str,
#        },
#    }
#    logging.info(feature)
#    return feature


def get_dropdown_options():
    with open("array_classification.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render()
    result = client.query(sql_query)

    column_names = ["array_class"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    df1 = df["array_class"].to_numpy()
    # logging.info(df1)
    return df1


def get_dropdown_collection():
    with open("array_collection.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render()
    result = client.query(sql_query)

    column_names = ["collections"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    df1 = df["collections"].to_numpy()
    # logging.info(df1)
    return df1


base_map = dl.Map(
    style={"height": "50vh", "zIndex": 10},
    id="map",
    center=[50.8503, 4.3517],
    zoom=2,
    children=[
        dl.TileLayer(),
        dl.LayerGroup(id="cm"),
        dl.LayerGroup(id="pg"),
    ],
)

YEAR_BOUND_INF, YEAR_BOUND_SUP, YEAR_INCREMENT = 1500, 2024, 50

date_range = dmc.RangeSlider(
    id="date_range",
    marks=[
        {"value": i, "label": i}
        for i in range(YEAR_BOUND_INF, YEAR_BOUND_SUP, YEAR_INCREMENT)
    ],
    min=YEAR_BOUND_INF,
    max=YEAR_BOUND_SUP,
    value=[2000, 2024],
    mb=30,
    mx=70,
)


layout = html.Div(
    children=[
        dmc.SimpleGrid(
            cols={"base": 1, "sm": 2, "lg": 2},
            spacing={"base": 10, "sm": "xl"},
            verticalSpacing={"base": "md", "sm": "xl"},
            children=[
                html.Div(base_map),
                dmc.Stack(
                    [
                        dcc.Graph(id="count_per_year"),
                        date_range,
                    ]
                ),
            ],
            mb=30,
        ),
        dmc.SimpleGrid(
            cols={"base": 3, "sm": 1, "lg": 3},
            spacing={"base": 10, "sm": "xl"},
            verticalSpacing={"base": "md", "sm": "xl"},
            children=[
                html.Div(id="top_collectioncode"),
                html.Div(id="top_scientificname"),
                html.Div(id="top_recordedby"),
            ],
            mb=30,
        ),
        dmc.SimpleGrid(
            cols={"base": 1, "sm": 1, "lg": 1},
            spacing={"base": 10, "sm": "xl"},
            verticalSpacing={"base": "md", "sm": "xl"},
            children=[
                html.Div(id="datatable"),
            ],
            mb=30,
        ),
    ],
)


@callback(
    Output("datatable", "children"),
    Input("map", "bounds"),
    Input("date_range", "value"),
    Input("multi_select", "value"),
    Input("multi_select_collection", "value"),
    Input("text_input_recordedby", "value"),
)
def get_records(
    bounds, date_range, multi_select, multi_select_collection, text_input_recordedby
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    with open("get_records.sql", "r") as file:
        template = Template(file.read())
        # sql_query = template.render(recordedby=recordedby)
        sql_query = template.render(
            recordedby=text_input_recordedby,
            classification=multi_select,
            collection=multi_select_collection,
        )

    result = client.query(sql_query, parameters=parameters)
    column_names = [
        "year",
        "gbifid",
        "catalognumber",
        "species",
        "class",
        "recordedby",
        "occurrenceid",
        "collectioncode",
        "locality",
        "mediatype",
    ]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    # return html.Div([dash_table.DataTable(
    #        data=df.to_dict('records'),
    #        columns=[{'name': i, 'id': i} for i in df.columns],
    #        ),
    # ])

    # table = dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)
    # return table

    # Border color same as dmc.Table
    return dash_table.DataTable(
        data=df.to_dict("records"),
        columns=[{"name": i, "id": i} for i in df.columns],
        style_table={"overflowX": "auto"},
        style_cell={
            "whiteSpace": "normal",
            "height": "auto",
            "font-family": "'Inter', sans-serif",
            "font-size": "13px",
            "padding": "0 1rem",
            "text-align": "left",
            "border": "1px solid #dee2e6",
        },
        style_header={"fontWeight": "bold"},
    )


@callback(
    Output("top_scientificname", "children"),
    Input("map", "bounds"),
    Input("date_range", "value"),
    Input("multi_select", "value"),
    Input("multi_select_collection", "value"),
    Input("text_input_recordedby", "value"),
)
def top_scientificname(
    bounds, date_range, multi_select, multi_select_collection, text_input_recordedby
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    with open("top_scientificname.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            recordedby=text_input_recordedby,
            classification=multi_select,
            collection=multi_select_collection,
        )
    result = client.query(sql_query, parameters=parameters)

    column_names = ["Top scientificname", "count"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    table = dash_table.DataTable(
        data=df.to_dict("records"),
        columns=[{"name": i, "id": i} for i in df.columns],
        style_table={"overflowX": "auto"},
        style_cell={
            "whiteSpace": "normal",
            "height": "auto",
            "font-family": "'Inter', sans-serif",
            "font-size": "13px",
            "padding": "0 1rem",
            "text-align": "left",
            "border": "1px solid #dee2e6",
        },
        style_header={"fontWeight": "bold"},
    )

    return table


@callback(
    Output("top_recordedby", "children"),
    Input("map", "bounds"),
    Input("date_range", "value"),
    Input("multi_select", "value"),
    Input("multi_select_collection", "value"),
    Input("text_input_recordedby", "value"),
)
def top_recordedby(
    bounds, date_range, multi_select, multi_select_collection, text_input_recordedby
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    with open("top_recordedby.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            recordedby=text_input_recordedby,
            classification=multi_select,
            collection=multi_select_collection,
        )
    result = client.query(sql_query, parameters=parameters)

    column_names = ["Top recordedby", "count"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    table = dash_table.DataTable(
        data=df.to_dict("records"),
        columns=[{"name": i, "id": i} for i in df.columns],
        style_table={"overflowX": "auto"},
        style_cell={
            "whiteSpace": "normal",
            "height": "auto",
            "font-family": "'Inter', sans-serif",
            "font-size": "13px",
            "padding": "0 1rem",
            "text-align": "left",
            "border": "1px solid #dee2e6",
        },
        style_header={"fontWeight": "bold"},
    )

    return table


@callback(
    Output("top_collectioncode", "children"),
    Input("map", "bounds"),
    Input("date_range", "value"),
    Input("multi_select", "value"),
    Input("multi_select_collection", "value"),
    Input("text_input_recordedby", "value"),
)
def top_collectioncode(
    bounds, date_range, multi_select, multi_select_collection, text_input_recordedby
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    with open("top_collectioncode.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            recordedby=text_input_recordedby,
            classification=multi_select,
            collection=multi_select_collection,
        )
    result = client.query(sql_query, parameters=parameters)

    column_names = ["Top collectioncode", "count"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    table = dash_table.DataTable(
        data=df.to_dict("records"),
        columns=[{"name": i, "id": i} for i in df.columns],
        style_table={"overflowX": "auto"},
        style_cell={
            "whiteSpace": "normal",
            "height": "auto",
            "font-family": "'Inter', sans-serif",
            "font-size": "13px",
            "padding": "0 1rem",
            "text-align": "left",
            "border": "1px solid #dee2e6",
        },
        style_header={"fontWeight": "bold"},
    )

    return table


@callback(
    Output("cm", "children"),
    Output("pg", "children"),
    Input("map", "bounds"),
    Input("date_range", "value"),
    Input("multi_select", "value"),
    Input("multi_select_collection", "value"),
    Input("text_input_recordedby", "value"),
)
def get_clusters_in_bbox(
    bounds, date_range, multi_select, multi_select_collection, text_input_recordedby
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )
    with open("clusters_in_bbox.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            recordedby=text_input_recordedby,
            classification=multi_select,
            collection=multi_select_collection,
        )
    result = client.query(sql_query, parameters=parameters)

    # logging.info(sql_query)

    # Define column names
    column_names = [
        "cnt",
        "column_element",
        "latitude",
        "longitude",
        "h3_index",
        "boundary",
    ]

    # Convert the list of lists into a pandas DataFrame with specified column names
    df = pd.DataFrame(result.result_rows, columns=column_names)

    # for index, row in df.iterrows():
    # print(row['h3_2'])
    # print(type(row['boundary']))

    # pd.DataFrame.info(df)

    # logging.info(df["longitude"])

    # Add column "geojson_feature" to dataframe
    # df["geojson_feature"] = df["h3_index"].apply(get_geojson_feature)

    # Add column "geojson_feature" to dataframe
    df["radius"] = 3 + (df["cnt"] * (50 / (df["cnt"].sum() + 1)))

    # logging.info(df)
    # logging.info(df["radius"])

    cm = [
        dl.CircleMarker(
            center=[row["latitude"], row["longitude"]],
            radius=row["radius"],
            fill=True,
            fillOpacity=0.7,
            children=[
                dl.Tooltip(f"Number of objects: {row['cnt']}"),
            ],
        )
        for index, row in df.iterrows()
    ]

    pg = [
        dl.Polygon(
            positions=row["boundary"],
            color="green",
            opacity=0.1,
            fillOpacity=0.1,
            weight=2,
            children=[
                # dl.Tooltip(f"Index: {row['h3_index']}"),
                dl.Popup(f"Index: {row['h3_index']}")
            ],
        )
        for index, row in df.iterrows()
    ]
    # logging.info(pg)
    return cm, pg
    # return cm


@callback(
    Output("count_per_year", "figure"),
    Input("map", "bounds"),
    Input("date_range", "value"),
    Input("multi_select", "value"),
    Input("multi_select_collection", "value"),
    Input("text_input_recordedby", "value"),
)
def get_count_per_year(
    bounds, date_range, multi_select, multi_select_collection, text_input_recordedby
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )
    with open("count_per_year.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            recordedby=text_input_recordedby,
            classification=multi_select,
            collection=multi_select_collection,
        )
    result = client.query(sql_query, parameters=parameters)

    column_names = ["year", "count"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    fig = px.bar(df, x="year", y="count", barmode="group", custom_data="year")
    return fig
