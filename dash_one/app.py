import dash
import dash_mantine_components as dmc
from dash_iconify import DashIconify
from dash import (
    Dash,
    _dash_renderer,
    html,
    dcc,
    callback,
    Input,
    Output,
    State,
    ctx,
    page_container,
    page_registry,
)
import os
import dash_leaflet as dl
from dash import dash_table
from dash.dependencies import Input, Output
import json
import logging
import pandas as pd
import clickhouse_connect
from clickhouse_connect import common
from clickhouse_connect.driver import httputil
import datetime as dt
from jinja2 import Template
import plotly.express as px
import numpy

_dash_renderer._set_react_version("18.2.0")


# Configure logging to print to console
logging.basicConfig(level=logging.INFO)

# ClickHouse configurations
clickhouse_host = os.getenv("CLICKHOUSE_HOST")
clickhouse_user = os.getenv("CLICKHOUSE_USER")
clickhouse_pass = os.getenv("CLICKHOUSE_PASSWORD")

# Clickhouse multi query
common.set_setting("autogenerate_session_id", False)

# Clickhouse pool manager
big_pool_mgr = httputil.get_pool_manager(maxsize=16, num_pools=12)

client = clickhouse_connect.get_client(
    pool_mgr=big_pool_mgr,
    host=clickhouse_host,
    port=8123,
    user=clickhouse_user,
    password=clickhouse_pass,
    database="db1",
)


def get_dropdown_options():
    with open("array_classification.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render()
    result = client.query(sql_query)

    column_names = ["array_class"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    df1 = df["array_class"].to_numpy()
    # logging.info(df1)
    return df1


def get_dropdown_collection():
    with open("array_collection.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render()
    result = client.query(sql_query)

    column_names = ["collections"]
    df = pd.DataFrame(result.result_rows, columns=column_names)

    df1 = df["collections"].to_numpy()
    # logging.info(df1)
    return df1


icons = {
    "github": "ion:logo-github",
    "tools": "bi:tools",
}


# df = pd.read_csv('https://raw.githubusercontent.com/plotly/datasets/master/solar.csv')

# table_a = dash_table.DataTable(
#    data = df.to_dict('records'),
#    columns=[{"name": i, "id": i} for i in df.columns]
#    )

# dmc_table = dmc.Table(
#    striped=True,
#    highlightOnHover=True,
#    #withBorder=True,
#    withColumnBorders=True,
#    data=df.to_dict('records')
# )

# dmc_table = dmc.Table(
#    data={
#        "caption": "Some elements from periodic table",
#        "head": ["Element position", "Atomic mass", "Symbol", "Element name"],
#        "body": [
#            [6, 12.011, "C", "Carbon"],
#            [7, 14.007, "N", "Nitrogen"],
#            [39, 88.906, "Y", "Yttrium"],
#            [56, 137.33, "Ba", "Barium"],
#            [58, 140.12, "Ce", "Cerium"],
#        ],
#    }
# )


# base_map = dl.Map(style={'height': '50vh', 'zIndex': 10}, id="map", center=[50.8503, 4.3517], zoom=2, children=[
#     dl.TileLayer(),
#     dl.LayerGroup(id="df_clusters"),
# ])
#
#
multi_select_classification = dmc.MultiSelect(
    label="Select classification",
    data=get_dropdown_options(),
    # data=["React", "Angular", "Svelte", "Vue"],
    limit=500,
    searchable=True,
    # w=400,
    nothingFoundMessage="Nothing Found!",
    mb=30,
    id="multi_select",
)

multi_select_collection = dmc.MultiSelect(
    label="Select collection",
    data=get_dropdown_collection(),
    limit=500,
    searchable=True,
    nothingFoundMessage="Nothing Found!",
    mb=30,
    id="multi_select_collection",
)

# Alternative for dmc.MultiSelect:
# multi_select_collection = dcc.Dropdown(
#    placeholder="Select collection...",
#    id="multi_select_collection",
#    options=get_dropdown_collection(),
#    style={
#        "margin-bottom": "30px",
#        "font-size": "13px",
#        # "border": "1px solid #CDD4DA",
#        #'border-radius': '3px',
#    },
#    multi=True,
# )


#
#
# YEAR_BOUND_INF, YEAR_BOUND_SUP, YEAR_INCREMENT = 1500, 2024, 50
#
# date_range = dmc.RangeSlider(
#    id="date_range",
#    marks=[
#        {'value': i, 'label': i} for i in range(YEAR_BOUND_INF, YEAR_BOUND_SUP, YEAR_INCREMENT)
#    ],
#    min=YEAR_BOUND_INF,
#    max=YEAR_BOUND_SUP,
#    value=[2000, 2024],
#    mb=30,
# )


# set_frame_color = dmc.Checkbox(
#    id="checkbox-set-background", label="Set image frame color", checked=True, mb=10
# )


def create_link(icon, href, text=""):
    return dmc.Anchor(
        [
            (
                dmc.ActionIcon(
                    DashIconify(icon=icon, width=25), variant="transparent", size="lg"
                )
                if icon
                else None
            ),
            text,
        ],
        href=href,
        target="_blank",
    )


burger_button = dcc.Loading(
    dmc.Burger(id="burger-button", opened=False, hiddenFrom="md"),
    overlay_style={"zIndex": 5000},
    delay_show=500,
    custom_spinner=dmc.Group(dmc.Loader(type="dots", size="sm")),
)

header = dmc.Group(
    [
        burger_button,
        dmc.Image(src="/assets/Naturalis_logo_volledig_rood.jpg", h=36, w="100%"),
        dmc.Text(["Explore"], size="xl", fw=700),
        dmc.Text("Explore GBIF datasets", visibleFrom="sm", size="xl"),
        dmc.Text(
            create_link(icons["github"], "https://registry.opendata.aws/gbif/"),
            ml="auto",
        ),
    ],
    justify="flex-start",
    gap="sm",
    style={"height": "1 !important"},
)


navbar = dmc.ScrollArea(
    [
        multi_select_classification,
        multi_select_collection,
        # dmc.TextInput(
        #    id="text_input_recordedby",
        #    debounce=10000,
        #    label="Recorded by",
        #    mb=30,
        # ),
        dcc.Input(
            id="text_input_recordedby",
            placeholder="Recorded by...",
            type="text",
            debounce=True,
            # className="mantine-TextInput-input",
            style={
                "width": "100%",
                "border": "1px solid #CDD4DA",
                "border-radius": "3px",
                "font-size": "13px",
                "height": "35px",
                "padding-left": "12px",
            },
        ),
    ],
    offsetScrollbars=True,
    type="scroll",
    style={"height": "100%"},
)


app = Dash(__name__, use_pages=True)


app.layout = dmc.MantineProvider(
    [
        dmc.AppShell(
            [
                dmc.AppShellHeader(
                    [dmc.Space(h=5), header], px=25, style={"height": "50px"}
                ),
                dmc.AppShellNavbar(navbar, p=24, style={"top": "50px"}),
                dmc.AppShellMain(page_container),
            ],
            header={"height": 70},
            padding="xl",
            navbar={
                "width": 375,
                "breakpoint": "md",
                "collapsed": {"mobile": True},
            },
            id="app-shell",
        )
    ],
    deduplicateCssVariables=False,
)


# @callback(
#    Output("datatable", "children"),
#    Input("map", "bounds"),
#    Input("date_range", "value"),
#    Input("multi_select", "value"),
#    Input("multi_select_collection", "value")
# )
# def get_records(bounds, date_range, multi_select, multi_select_collection, recordedby=None):
#
#    if bounds is None:
#      bounds = [[-90, -180], [90, 180]]
#
#    parameters = (bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], date_range[0], date_range[1])
#
#    with open('get_records.sql', 'r') as file:
#        template = Template(file.read())
#        #sql_query = template.render(recordedby=recordedby)
#        sql_query = template.render(recordedby=recordedby, classification=multi_select, collection=multi_select_collection)
#
#    result = client.query(sql_query, parameters=parameters)
#    column_names = ['year', 'gbifid', 'catalognumber', 'species', 'class', 'recordedby', 'occurrenceid', 'collectioncode', 'locality', 'mediatype']
#    df = pd.DataFrame(result.result_rows, columns=column_names)
#
#    #return html.Div([dash_table.DataTable(
#    #        data=df.to_dict('records'),
#    #        columns=[{'name': i, 'id': i} for i in df.columns],
#    #        ),
#    #])
#
#    #table = dbc.Table.from_dataframe(df, striped=True, bordered=True, hover=True)
#    #return table
#
#    # Border color same as dmc.Table
#    return dash_table.DataTable(
#               data=df.to_dict('records'),
#               columns=[{'name': i, 'id': i} for i in df.columns],
#               style_table={'overflowX': 'auto'},
#               style_cell={
#                       'whiteSpace': 'normal',
#                       'height': 'auto',
#                       "font-family": "'Inter', sans-serif",
#                       'font-size': '14px',
#                       "padding": "0 1rem",
#                       'text-align': 'left',
#                       'border': '1px solid #dee2e6'
#               },
#               style_header={
#                   'fontWeight': 'bold'
#               },
#           )

# @callback(
#    Output("top_scientificname", "children"),
#    Input("map", "bounds"),
#    Input("date_range", "value"),
#    Input("multi_select", "value"),
#    Input("multi_select_collection", "value")
# )
# def top_scientificname(bounds, date_range, multi_select, multi_select_collection, recordedby=None):
#
#    if bounds is None:
#      bounds = [[-90, -180], [90, 180]]
#
#    parameters = (bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], date_range[0], date_range[1])
#
#    with open('top_scientificname.sql', 'r') as file:
#        template = Template(file.read())
#        sql_query = template.render(recordedby=recordedby, classification=multi_select, collection=multi_select_collection)
#    result = client.query(sql_query, parameters=parameters)
#
#    column_names = ['Top scientificname', 'count']
#    df = pd.DataFrame(result.result_rows, columns=column_names)
#
#    table = dash_table.DataTable(
#               data=df.to_dict('records'),
#               columns=[{'name': i, 'id': i} for i in df.columns],
#               style_table={'overflowX': 'auto'},
#               style_cell={
#                       'whiteSpace': 'normal',
#                       'height': 'auto',
#                       "font-family": "'Inter', sans-serif",
#                       'font-size': '14px',
#                       "padding": "0 1rem",
#                       'text-align': 'left',
#                       'border': '1px solid #dee2e6'
#               },
#               style_header={
#                   'fontWeight': 'bold'
#               },
#           )
#
#    return table

# @callback(
#    Output("top_recordedby", "children"),
#    Input("map", "bounds"),
#    Input("date_range", "value"),
#    Input("multi_select", "value"),
#    Input("multi_select_collection", "value")
# )
# def top_recordedby(bounds, date_range, multi_select, multi_select_collection, recordedby=None):
#
#    if bounds is None:
#      bounds = [[-90, -180], [90, 180]]
#
#    parameters = (bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], date_range[0], date_range[1])
#
#    with open('top_recordedby.sql', 'r') as file:
#        template = Template(file.read())
#        sql_query = template.render(recordedby=recordedby, classification=multi_select, collection=multi_select_collection)
#    result = client.query(sql_query, parameters=parameters)
#
#    column_names = ['Top recordedby', 'count']
#    df = pd.DataFrame(result.result_rows, columns=column_names)
#
#    table = dash_table.DataTable(
#               data=df.to_dict('records'),
#               columns=[{'name': i, 'id': i} for i in df.columns],
#               style_table={'overflowX': 'auto'},
#               style_cell={
#                       'whiteSpace': 'normal',
#                       'height': 'auto',
#                       "font-family": "'Inter', sans-serif",
#                       'font-size': '14px',
#                       "padding": "0 1rem",
#                       'text-align': 'left',
#                       'border': '1px solid #dee2e6'
#               },
#               style_header={
#                   'fontWeight': 'bold'
#               },
#           )
#
#    return table

# @callback(
#    Output("top_collectioncode", "children"),
#    Input("map", "bounds"),
#    Input("date_range", "value"),
#    Input("multi_select", "value"),
#    Input("multi_select_collection", "value")
# )
# def top_collectioncode(bounds, date_range, multi_select, multi_select_collection, recordedby=None):
#
#    if bounds is None:
#      bounds = [[-90, -180], [90, 180]]
#
#    parameters = (bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], date_range[0], date_range[1])
#
#    with open('top_collectioncode.sql', 'r') as file:
#        template = Template(file.read())
#        sql_query = template.render(recordedby=recordedby, classification=multi_select, collection=multi_select_collection)
#    result = client.query(sql_query, parameters=parameters)
#
#    column_names = ['Top collectioncode', 'count']
#    df = pd.DataFrame(result.result_rows, columns=column_names)
#
#    table = dash_table.DataTable(
#               data=df.to_dict('records'),
#               columns=[{'name': i, 'id': i} for i in df.columns],
#               style_table={'overflowX': 'auto'},
#               style_cell={
#                       'whiteSpace': 'normal',
#                       'height': 'auto',
#                       "font-family": "'Inter', sans-serif",
#                       'font-size': '14px',
#                       "padding": "0 1rem",
#                       'text-align': 'left',
#                       'border': '1px solid #dee2e6'
#               },
#               style_header={
#                   'fontWeight': 'bold'
#               },
#           )
#
#    return table

# @callback(
#    Output("df_clusters", "children"),
#    Input("map", "bounds"),
#    Input("date_range", "value"),
#    Input("multi_select", "value"),
#    Input("multi_select_collection", "value")
# )
# def get_clusters_in_bbox(bounds, date_range, multi_select, multi_select_collection, recordedby=None):
#
#    if bounds is None:
#      bounds = [[-90, -180], [90, 180]]
#
#    parameters = (bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], date_range[0], date_range[1], bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], date_range[0], date_range[1])
#    with open('clusters_in_bbox.sql', 'r') as file:
#        template = Template(file.read())
#        sql_query = template.render(recordedby=recordedby, classification=multi_select, collection=multi_select_collection)
#    result = client.query(sql_query, parameters=parameters)
#
#    # Define column names
#    column_names = ['cnt', 'column_element', 'latitude', 'longitude']
#
#    # Convert the list of lists into a pandas DataFrame with specified column names
#    df = pd.DataFrame(result.result_rows, columns=column_names)
#
#    #logging.info(multi_select)
#    #logging.info(bounds)
#
#    return [dl.CircleMarker(center=[row['latitude'], row['longitude']],
#        radius=5,
#        fill=True,
#        fillOpacity=0.7,
#        children=[
#            dl.Tooltip(
#                f"Number of objects: {row['cnt']}")
#            ])
#            for index, row in df.iterrows()
#    ]


# @callback(
#    Output("count_per_year", "figure"),
#    Input("map", "bounds"),
#    Input("date_range", "value"),
#    Input("multi_select", "value"),
#    Input("multi_select_collection", "value")
# )
# def get_count_per_year(bounds, date_range, multi_select, multi_select_collection, recordedby=None):
#
#    if bounds is None:
#      bounds = [[-90, -180], [90, 180]]
#
#    parameters = (bounds[0][0], bounds[1][0], bounds[0][1], bounds[1][1], date_range[0], date_range[1])
#    with open('count_per_year.sql', 'r') as file:
#        template = Template(file.read())
#        sql_query = template.render(recordedby=recordedby, classification=multi_select, collection=multi_select_collection)
#    result = client.query(sql_query, parameters=parameters)
#
#    column_names = ['year', 'count']
#    df = pd.DataFrame(result.result_rows, columns=column_names)
#
#    fig = px.bar(df, x="year", y="count",
#         barmode="group")
#
#    return fig


@callback(
    Output("app-shell", "navbar"),
    Input("burger-button", "opened"),
    State("app-shell", "navbar"),
)
def navbar_is_open(opened, navbar):
    navbar["collapsed"] = {"mobile": not opened}
    return navbar


# On mobile close the navbar on update
@callback(
    Output("burger-button", "opened"),
    Input("map", "bounds"),
    Input("date_range", "value"),
    Input("multi_select", "value"),
    Input("multi_select_collection", "value"),
    prevent_initial_call=True,
)
def navbar_is_open(*_):
    return False


if __name__ == "__main__":
    app.run_server(debug=True, host="0.0.0.0")
