SELECT year, gbifid, catalognumber, species, class, recordedby_string AS recordedby, occurrenceid, collectioncode, locality, arrayStringConcat(mediatype.array_element) AS mediatype FROM gbif_enriched_nl
WHERE decimallatitude BETWEEN %s AND %s AND decimallongitude BETWEEN %s AND %s
AND year BETWEEN %s AND %s
{% if recordedby %}
AND match(lower(recordedby_string), lower('{{ recordedby }}'))
{% endif %}
{% if classification %}
AND hasAny(array(class), {{ classification }})
{% endif %}
{% if collection %}
AND hasAny(array(collectioncode), {{ collection }})
{% endif %}
ORDER BY year DESC
LIMIT 100
