SELECT collectioncode, count(*) FROM gbif_enriched_nl 
WHERE decimallatitude BETWEEN %s AND %s AND decimallongitude BETWEEN %s AND %s
AND year BETWEEN %s AND %s
{% if recordedby %}
AND match(lower(recordedby_string), lower('{{ recordedby }}'))
{% endif %}
{% if classification %}
AND hasAny(array(class), {{ classification }})
{% endif %}
{% if collection %}    
AND hasAny(array(collectioncode), {{ collection }})         
{% endif %}
GROUP BY 1 ORDER BY 2 DESC LIMIT 10
