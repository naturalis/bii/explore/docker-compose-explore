--SELECT groupUniqArray(class) AS unique_values_class
SELECT DISTINCT class AS unique_values_class FROM gbif_enriched_nl WHERE class IS NOT NULL
--WITH a as (SELECT DISTINCT class AS unique_values_class
--FROM gbif_enriched) select array(unique_values_class) from a

