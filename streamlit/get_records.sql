SELECT year, gbifid, catalognumber, species, class, arrayStringConcat(recordedby.array_element) AS recordedby, occurrenceid, collectioncode, locality, decimallatitude, decimallongitude, arrayStringConcat(mediatype.array_element) AS mediatype FROM gbif_enriched
WHERE decimallatitude BETWEEN %s AND %s AND decimallongitude BETWEEN %s AND %s
AND year BETWEEN %s AND %s
{% if recordedby %}
AND match(lower(arrayStringConcat(recordedby.array_element)), lower('{{ recordedby }}'))
{% endif %}
{% if scientificname %}
AND match(lower(scientificname), lower('{{ scientificname }}'))
{% endif %}
{% if classification %}
AND hasAny(array(class), {{ classification }})
{% endif %}
ORDER BY year ASC
LIMIT 100
