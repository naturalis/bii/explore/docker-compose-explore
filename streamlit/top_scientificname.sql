SELECT scientificname, count(*) FROM gbif_enriched 
WHERE decimallatitude BETWEEN %s AND %s AND decimallongitude BETWEEN %s AND %s
AND year BETWEEN %s AND %s
{% if recordedby %}
AND match(lower(arrayStringConcat(recordedby.array_element)), lower('{{ recordedby }}'))
{% endif %}
{% if scientificname %}
AND match(lower(scientificname), lower('{{ scientificname }}'))
{% endif %}
{% if classification %}
AND hasAny(array(class), {{ classification }})
{% endif %}
GROUP BY 1 ORDER BY 2 DESC LIMIT 10

