import plotly.express as px
import streamlit as st

df = px.data.gapminder()

#fig = px.scatter(
#    df.query("year==2007"),
#    x="gdpPercap",
#    y="lifeExp",
#    size="pop",
#    color="continent",
#    hover_name="country",
#    log_x=True,
#    size_max=60,
#)



fig = px.scatter(
            df,
            x="gdpPercap",
            y="lifeExp",
            animation_frame="year",
            animation_group="country",
            size="pop",
            color="continent",
            hover_name="country",
            log_x=True,
            size_max=55,
            range_x=[100, 100000],
            range_y=[25, 90]
)




tab1, tab2 = st.tabs(["Streamlit theme (default)", "Plotly native theme"])
with tab1:
    # Use the Streamlit theme.
    # This is the default. So you can also omit the theme argument.
    st.plotly_chart(fig, theme="streamlit", use_container_width=True)
with tab2:
    # Use the native Plotly theme.
    st.plotly_chart(fig, theme=None, use_container_width=True)
