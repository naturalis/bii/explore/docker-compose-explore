import folium
import streamlit as st
import pandas as pd
import clickhouse_connect
from streamlit_folium import st_folium
import os
import datetime as dt
from jinja2 import Template
import logging
import numpy
import plotly.express as px

# Configure logging to print to console
logging.basicConfig(level=logging.INFO)

# Streamlit settings
st.set_page_config(
    page_title="streamlit-folium",
    page_icon=":world_map:~O",
    layout="wide"
)

# ClickHouse configurations
clickhouse_host = os.getenv('CLICKHOUSE_HOST')
clickhouse_user = os.getenv('CLICKHOUSE_USER')
clickhouse_pass = os.getenv('CLICKHOUSE_PASSWORD')
client = clickhouse_connect.get_client(host=clickhouse_host, port=8123, user=clickhouse_user, password=clickhouse_pass, database='db1')

@st.cache_resource
def clickhouse():
    client = clickhouse_connect.get_client(host=clickhouse_host, port=8123, user=clickhouse_user, password=clickhouse_pass, database='db1')
    return client

clickhouse()

def init_map_and_get_bounds():
    start_bounds = {
        '_southWest': {'lat': -90, 'lng': -255},
        '_northEast': {'lat': 90, 'lng': 255}
    }

    session_keys = list(st.session_state.keys())

    # Init map
    m = folium.Map()

    # Check if session state returns an empty list
    if session_keys == []:
        bounds = start_bounds
    else:
        bounds = st.session_state[session_keys[0]]['bounds']

    # Get corners of the bounding box
    southwest_lat = bounds["_southWest"]["lat"]
    southwest_lng = bounds["_southWest"]["lng"]
    northeast_lat = bounds["_northEast"]["lat"]
    northeast_lng = bounds["_northEast"]["lng"]
    bbox = [southwest_lat, southwest_lng, northeast_lat, northeast_lng]

    return m, bbox


def get_clusters_in_bbox(bbox, start_year, end_year, recordedby, scientificname, classification):
    parameters = (bbox[0], bbox[2], bbox[1], bbox[3], start_year, end_year, bbox[0], bbox[2], bbox[1], bbox[3], start_year, end_year)
    with open('clusters_in_bbox.sql', 'r') as file:
        template = Template(file.read())
        sql_query = template.render(recordedby=recordedby, scientificname=scientificname, classification=classification)
    result = client.query(sql_query, parameters=parameters)

    # Define column names
    column_names = ['count', 'column_element', 'latitude', 'longitude']

    # Convert the list of lists into a pandas DataFrame with specified column names
    df_clusters = pd.DataFrame(result.result_rows, columns=column_names)

    #logging.info(sql_query)
    return df_clusters


def get_records(bbox, start_year, end_year, recordedby, scientificname, classification):
    parameters = (bbox[0], bbox[2], bbox[1], bbox[3], start_year, end_year)

    with open('get_records.sql', 'r') as file:
        template = Template(file.read())
        sql_query = template.render(recordedby=recordedby, scientificname=scientificname, classification=classification)

    result = client.query(sql_query, parameters=parameters)
    column_names = ['year', 'gbifid', 'catalognumber', 'species', 'class', 'recordedby', 'occurrenceid', 'collectioncode', 'locality', 'decimallatitude', 'decimallongitude', 'mediatype']
    df_get_records = pd.DataFrame(result.result_rows, columns=column_names)

    formatted_df = df_get_records.style.format({'year': '{:.0f}'})
    return formatted_df


def get_count_per_year(bbox, start_year, end_year, recordedby, scientificname, classification):
    parameters = (bbox[0], bbox[2], bbox[1], bbox[3], start_year, end_year)
    with open('count_per_year.sql', 'r') as file:
        template = Template(file.read())
        sql_query = template.render(recordedby=recordedby, scientificname=scientificname, classification=classification)
    result = client.query(sql_query, parameters=parameters)

    column_names = ['year', 'count']
    df_get_count_per_year = pd.DataFrame(result.result_rows, columns=column_names)

    formatted_df = df_get_count_per_year.style.format({'year': '{:.0f}'})

    return df_get_count_per_year

def top_recordedby(bbox, start_year, end_year, recordedby, scientificname, classification):
    parameters = (bbox[0], bbox[2], bbox[1], bbox[3], start_year, end_year)
    with open('top_recordedby.sql', 'r') as file:
        template = Template(file.read())
        sql_query = template.render(recordedby=recordedby, scientificname=scientificname, classification=classification)
    result = client.query(sql_query, parameters=parameters)

    column_names = ['recordedby', 'count']
    df_top_recordedby = pd.DataFrame(result.result_rows, columns=column_names)

    #logging.info(df_top_recordedby)
    return df_top_recordedby

def top_scientificname(bbox, start_year, end_year, recordedby, scientificname, classification):
    parameters = (bbox[0], bbox[2], bbox[1], bbox[3], start_year, end_year)
    with open('top_scientificname.sql', 'r') as file:
        template = Template(file.read())
        sql_query = template.render(recordedby=recordedby, scientificname=scientificname, classification=classification)
    result = client.query(sql_query, parameters=parameters)

    column_names = ['scientificname', 'count']
    df_top_scientificname = pd.DataFrame(result.result_rows, columns=column_names)

    #logging.info(df_top_scientificname)
    return df_top_scientificname

# Function to create featuregroup
def create_featuregroup(cluster_markers):
    total_count = cluster_markers['count'].sum()
    scale_factor = 50 / (total_count + 1)
    fg = folium.FeatureGroup(name="Markers")
    for index, row in cluster_markers.iterrows():
        # Create a CircleMarker for each row
        folium.CircleMarker(
            location=[row['latitude'], row['longitude']],
            radius= 4 + (row['count'] * scale_factor),
            tooltip=int(row['count']),
            color='cornflowerblue',
            fill=True,
            fill_opacity=1,
            opacity=1,
        ).add_to(fg)
    return fg, total_count

@st.cache_data
def multiselect_class():
    with open('array_class.sql', 'r') as file:
        template = Template(file.read())
        sql_query = template.render()
    result = client.query(sql_query)

    column_names = ['array_class']
    df = pd.DataFrame(result.result_rows, columns=column_names)

    #logging.info(df['array_class'].to_numpy())
    return df['array_class'].to_numpy()


# User input
#with col1:
with st.sidebar:
    st.header('Filters')
    start_year = st.text_input('Enter Start Year:', '1501')
    end_year = st.text_input('Enter End Year:', '2024')
    recordedby = st.text_input('Recorded by')
    scientificname = st.text_input('Scientificname')
    classification = st.multiselect(
        label = 'Classification',
        options = multiselect_class(),
        default = [])

# Run functions
m, bbox = init_map_and_get_bounds()
cluster_markers = get_clusters_in_bbox(bbox, start_year, end_year, recordedby, scientificname, classification)
fg, total_count = create_featuregroup(cluster_markers)
result_get_records = get_records(bbox, start_year, end_year, recordedby, scientificname, classification)
result_df_get_count_per_year = get_count_per_year(bbox, start_year, end_year, recordedby, scientificname, classification)
result_top_recordedby = top_recordedby(bbox, start_year, end_year, recordedby, scientificname, classification)
result_top_scientificname = top_scientificname(bbox, start_year, end_year, recordedby, scientificname, classification)


# Output
st.title('Explore')

# Top row
cont1 = st.container(border=True)

cola1, cola2, cola3 = st.columns([1,1,5])

with cont1:
    with cola1:
        st.metric("Total markers:", total_count)
    with cola2:
        st.write("Col 2")
    with cola3:
        st.write("Col 3")

# Second row
cont2 = st.container(border=True)

# Use two columns
colb1, colb2 = st.columns(2)

with cont2:
    with colb1:
        # Plot map using streamlit
        map = st_folium(m,
            center=[20,60],
            zoom=1,
            height=400,
            width=900,
            key="map",
            feature_group_to_add = fg,
        )
    with colb2:
        st.bar_chart(result_df_get_count_per_year, x='year', y='count', height=400)

# Third row
cont3 = st.container(border=True)

# Use two columns
colc1, colc2, colc3 = st.columns(3)

def func_piet():
    piet = st.write("Another panel")
    return piet

with cont3:
    with colc1:
        st.dataframe(result_top_recordedby,
        column_config={
            "recordedby": st.column_config.Column(
                width= "medium"
                ),
            "count": st.column_config.Column(
                width= "medium"
                )
            }
        )
    with colc2:
        st.dataframe(result_top_scientificname)
    with colc3:
        #st.write("Bla")
        func_piet()

# Table
st.dataframe(result_get_records)


# Cluster markers
st.dataframe(cluster_markers)

fig = px.scatter_geo(cluster_markers,
                     lat='latitude',
                     lon='longitude',
                     #scope='usa',
                     #color="count",
                     #size='count',
                     #projection="albers usa",
                     fitbounds = 'locations'
                     #animation_frame="datetime", 
                     #title='Your title'
                     )


st.plotly_chart(fig, theme="streamlit", use_container_width=True)

