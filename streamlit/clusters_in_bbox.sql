WITH d AS (
  WITH ( WITH
    a AS (
       SELECT
         [uniq(h3_1), uniq(h3_3), uniq(h3_5), uniq(h3_7), uniq(h3_9), uniq(h3_11), uniq(h3_13), uniq(h3_15)] AS number_of_cells,
         range(1, 9) AS column_range
         FROM gbif_enriched
         WHERE decimallatitude BETWEEN %s AND %s AND decimallongitude BETWEEN %s AND %s
         AND year BETWEEN %s AND %s
         {% if recordedby %}
         AND match(lower(arrayStringConcat(recordedby.array_element)), lower('{{ recordedby }}'))
         {% endif %}
         {% if scientificname %}
         AND match(lower(scientificname), lower('{{ scientificname }}'))
         {% endif %}
         {% if classification %}
	 AND hasAny(array(class), {{ classification }})
         {% endif %}
       ), b AS (
       SELECT *
         FROM a
         ARRAY JOIN column_range, number_of_cells), c AS (
         SELECT column_range,
         abs(number_of_cells - 500) AS diff
         FROM b
         ORDER BY
         2 ASC,
         1 DESC LIMIT 1) 
   SELECT column_range FROM c) AS column_element
   SELECT count(*) AS cnt, column_element, arrayElement([h3_1, h3_3, h3_5, h3_7, h3_9, h3_11, h3_13, h3_15], column_element) AS h3_index FROM gbif_enriched
   WHERE decimallatitude BETWEEN %s AND %s AND decimallongitude BETWEEN %s AND %s
   AND year BETWEEN %s AND %s
   {% if recordedby %}
   AND match(lower(arrayStringConcat(recordedby.array_element)), lower('{{ recordedby }}'))
   {% endif %}
   {% if scientificname %}
   AND match(lower(scientificname), lower('{{ scientificname }}'))
   {% endif %}
   {% if classification %}
   AND hasAny(array(class), {{ classification }})
   {% endif %}
   GROUP BY 2,3)
  SELECT
  cnt,
  column_element,
  h3ToGeo(assumeNotNull(h3_index)).2 AS latitude,
  h3ToGeo(assumeNotNull(h3_index)).1 AS longitude
FROM d

