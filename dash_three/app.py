# https://community.plotly.com/t/dash-mantine-components-0-14-1/83865/26?page=2

import dash_mantine_components as dmc
from dash_iconify import DashIconify
from dash import (
    Dash,
    _dash_renderer,
    dcc,
    callback,
    Input,
    Output,
    State,
    page_container,
    page_registry,
)

_dash_renderer._set_react_version("18.2.0")


theme_toggle = dmc.ActionIcon(
    [
        dmc.Paper(DashIconify(icon="radix-icons:sun", width=25), darkHidden=True),
        dmc.Paper(DashIconify(icon="radix-icons:moon", width=25), lightHidden=True),
    ],
    variant="transparent",
    color="yellow",
    id="color-scheme-toggle",
    size="lg",
    ms="auto",
)

header = dmc.Group(
    [
        dmc.Burger(id="burger-button", opened=False, hiddenFrom="md"),
        dmc.Image(src="/assets/logo_Naturalis_rood_rgb_249-66-58.png", h=36, w="100%"),
        dmc.Text(["Explore"], size="xl", fw=700, mt=20),
        theme_toggle,
    ],
    justify="flex-start",
    gap="lg",
    style={"height": "1 !important"},
)

navbar = dcc.Loading(
    dmc.ScrollArea(
        [
            # dmc.Text("Your Sidebar content", fw=700),
        ],
        offsetScrollbars=True,
        type="scroll",
        style={"height": "100%"},
    ),
)

# page_content = [dmc.Text("Your page content"), dmc.Loader(color="red", size="md", variant="oval")]

stylesheets = [
    "https://unpkg.com/@mantine/dates@7/styles.css",
    "https://unpkg.com/@mantine/code-highlight@7/styles.css",
    "https://unpkg.com/@mantine/charts@7/styles.css",
    "https://unpkg.com/@mantine/carousel@7/styles.css",
    "https://unpkg.com/@mantine/notifications@7/styles.css",
    "https://unpkg.com/@mantine/nprogress@7/styles.css",
]

app = Dash(
    external_stylesheets=stylesheets, use_pages=True, suppress_callback_exceptions=True
)


app_shell = dmc.AppShell(
    [
        dmc.AppShellHeader(header, px=25),
        dmc.AppShellNavbar(navbar, p=24),
        # dmc.AppShellAside("Aside", withBorder=False),
        dmc.AppShellMain(page_container),
        dmc.AppShellFooter("Footer"),
    ],
    header={"height": 70},
    padding="xl",
    navbar={
        # "width": 350,
        "width": 350,
        "breakpoint": "md",
        "collapsed": {"mobile": True},
    },
    # aside={
    #     "width": 300,
    #     "breakpoint": "xl",
    #     "collapsed": {"desktop": False, "mobile": True},
    # },
    id="app-shell",
)

app.layout = dmc.MantineProvider(
    [dcc.Store(id="theme-store", storage_type="local", data="light"), app_shell],
    id="mantine-provider",
    forceColorScheme="light",
)


@callback(
    Output("app-shell", "navbar"),
    Input("burger-button", "opened"),
    State("app-shell", "navbar"),
)
def navbar_is_open(opened, navbar):
    navbar["collapsed"] = {"mobile": not opened}
    return navbar


@callback(
    Output("mantine-provider", "forceColorScheme"),
    Input("color-scheme-toggle", "n_clicks"),
    State("mantine-provider", "forceColorScheme"),
    prevent_initial_call=True,
)
def switch_theme(_, theme):
    return "dark" if theme == "light" else "light"


if __name__ == "__main__":
    app.run_server(debug=True, port=8050, host="0.0.0.0")
