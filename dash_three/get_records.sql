SELECT year, gbifid, catalognumber, species, class, recordedby_string AS recordedby, occurrenceid, collectioncode, locality, arrayStringConcat(mediatype.array_element) AS mediatype FROM db1.gbif_enriched
WHERE latitude BETWEEN %s AND %s 
AND longitude BETWEEN %s AND %s
AND year BETWEEN %s AND %s
ORDER BY year DESC
LIMIT 100;
