import dash
import dash_mantine_components as dmc

# from dash.exceptions import PreventUpdate
from dash import Dash, _dash_renderer, html, dcc, callback, Input, Output, State, ctx
import os
import dash_leaflet as dl
from dash import dash_table
from dash.dependencies import Input, Output
import json
import logging
import pandas as pd
import clickhouse_connect
from clickhouse_connect import common
from clickhouse_connect.driver import httputil
import datetime as dt
from jinja2 import Template
import plotly.express as px
import numpy
import psycopg
import h3
import plotly.graph_objects as go
from datetime import datetime
from dash.exceptions import PreventUpdate


dash.register_page(__name__, path="/")


# Configure logging to print to console
logging.basicConfig(level=logging.INFO)

# ClickHouse configurations
clickhouse_host = os.getenv("CLICKHOUSE_HOST")
clickhouse_db = os.getenv("CLICKHOUSE_DB")
clickhouse_user = os.getenv("CLICKHOUSE_USER")
clickhouse_pass = os.getenv("CLICKHOUSE_PASSWORD")


# Clickhouse multi query
common.set_setting("autogenerate_session_id", False)

# Clickhouse pool manager
big_pool_mgr = httputil.get_pool_manager(maxsize=16, num_pools=12)

client = clickhouse_connect.get_client(
    pool_mgr=big_pool_mgr,
    host=clickhouse_host,
    port=8123,
    user=clickhouse_user,
    password=clickhouse_pass,
    database=clickhouse_db,
)


# Postgres vars
postgres_host = os.getenv("POSTGRES_HOST")
postgres_port = os.getenv("POSTGRES_PORT")
postgres_db = os.getenv("POSTGRES_DB")
postgres_user = os.getenv("POSTGRES_USER")
postgres_password = os.getenv("POSTGRES_PASSWORD")


# Postgres config
def create_connection():
    conn = psycopg.connect(
        dbname=postgres_db,
        user=postgres_user,
        password=postgres_password,
        host=postgres_host,
        port=postgres_port,
    )
    return conn


# Postgres config
conn = create_connection()


# def get_geo_boundary(h3_index_str):
#     polygon = h3.cell_to_boundary(h3.str_to_int(h3_index_str))
#     # Check if any point in the polygon has latitude < -90 or latitude >= 90
#     if any(point[1] < -90 for point in polygon) and any(
#         point[1] >= 90 for point in polygon
#     ):
#         # Normalize the longitudes
#         return [
#             (point[0], point[1] + 360 if point[1] < 0 else point[1])
#             for point in polygon
#         ]
#     else:
#         return list(polygon)


PLOTLY_FMT = "%Y-%m-%d %H:%M:%S.%f"  # example: 2021-03-25 11:44:31.8968
PLOTLY_FMT_SEC = "%Y-%m-%d %H:%M:%S"  # example: 2021-03-25 11:44:31
PLOTLY_FMT_MIN = "%Y-%m-%d %H:%M"  # example: 2021-03-25 11:44
PLOTLY_FMT_DAY = "%Y-%m-%d"  # example: 2021-03-25


def plotly_to_datetime(plotly_string):
    fmts = [PLOTLY_FMT, PLOTLY_FMT_SEC, PLOTLY_FMT_MIN, PLOTLY_FMT_DAY]
    for fmt in fmts:
        try:
            return datetime.strptime(plotly_string, fmt).year
        except ValueError:
            continue
    return None
    # raise ValueError('Could not parse datetime from "{}"'.format(plotly_string))


# # Convert string H3 index to geographical boundary and convert to type list, has 180th meridian issue (example: 829b47fffffffff)
# def get_geo_boundary(h3_index_str):
#     # return list(h3_index_str)
#     logging.info(list([h3_index_str]))
#     return list([h3_index_str.lstrip('(').rstrip(')')])

layout = html.Div(
    children=[
        # dcc.Store(id='range-store', data={'xaxis.range': [1500, 2000]}),
        # dcc.Store(id="intermediate-value-date"),
        dmc.SimpleGrid(
            cols={"base": 1, "sm": 1, "lg": 1},
            spacing={"base": 10, "sm": "xl"},
            verticalSpacing={"base": "md", "sm": "xl"},
            children=[
                dmc.Select(
                    id="mode",
                    data=[
                        {
                            "value": "recordedby",
                            "label": "Datasets, count per cell, top count in table",
                        },
                        {
                            "value": "scientificname",
                            "label": "Scientific name, uniq per cell, top count in table",
                        },
                    ],
                    value="recordedby",
                    w=400,
                )
            ],
            mb=30,
        ),
        dmc.SimpleGrid(
            cols={"base": 1, "sm": 2, "lg": 2},
            spacing={"base": 10, "sm": "xl"},
            verticalSpacing={"base": "md", "sm": "xl"},
            children=[
                dl.Map(
                    style={"height": "50vh", "zIndex": 10},
                    id="map",
                    center=[44.00, 12.00],
                    zoom=2,
                    children=[
                        dl.TileLayer(),
                        dl.FullScreenControl(),
                        dl.LayerGroup(id="pg"),
                    ],
                ),
                dmc.Stack(
                    [
                        dcc.Graph(
                            id="count_per_year",
                            config={
                                "modeBarButtonsToRemove": [
                                    "select2d",
                                    "lasso2d",
                                ],  # Remove lasso and rectangle select
                                "displayModeBar": True,  # Display the mode bar
                            },
                        ),
                        dmc.Text(id="selected_date_range"),
                        dmc.Text(
                            "A nonexistent object was used in an `Output` of a Dash callback",
                            id="output-selected-range",
                            style={"display": "none"},
                        ),
                    ]
                ),
            ],
            mb=10,
        ),
        dmc.SimpleGrid(
            cols={"base": 2, "sm": 1, "lg": 2},
            spacing={"base": 10, "sm": "xl"},
            verticalSpacing={"base": "md", "sm": "xl"},
            children=[
                html.Div(id="top_recordedby"),
                # html.Div(id="top_dataset"),
                # html.Div(id="top_scientificname"),
            ],
            mb=30,
        ),
        dmc.SimpleGrid(
            cols={"base": 1, "sm": 1, "lg": 1},
            spacing={"base": 10, "sm": "xl"},
            verticalSpacing={"base": "md", "sm": "xl"},
            children=[
                html.Div(id="datatable"),
            ],
            mb=30,
        ),
    ],
)


@callback(
    Output("pg", "children"),
    Input("map", "bounds"),
    Input("output-selected-range", "children"),
    Input("mode", "value"),
)
def get_map_data(bounds, date_range, mode):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]
    else:
        bounds = bounds

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    # logging.info(f"date_range: {date_range}")

    with open("map_data.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            mode=mode,
        )

    result = client.query(sql_query, parameters=parameters)

    column_names = ["h3_2", "boundary", "cnt"]

    df = pd.DataFrame(result.result_rows, columns=column_names)

    if not df.empty:
        # Add additional column
        # df["h3_2_list"] = df["h3_2"].apply(get_geo_boundary)

        # Calculate opacity of cells
        df["opacity"] = df["cnt"] / (df["cnt"].max()).round(3) - 0.35

    pg = [
        dl.Polygon(
            # id="poly_id",
            positions=row["boundary"],
            color="green",
            opacity=0.15,
            weight=2,
            children=[
                # dl.Tooltip(f"Index: {row['h3_2']}"),
                # dl.Popup(f"Index: {row['h3_2']}, count: {row['cnt']}")
                dl.Popup(f"Count: {row['cnt']}")
            ],
            pathOptions={"fillOpacity": row["opacity"]},
        )
        for index, row in df.iterrows()
    ]
    # logging.info(sql_query)
    # logging.info(bounds)
    # logging.info(df)
    # logging.info(pg)
    # logging.info(len(df))
    # logging.info(df["cnt"].sum())

    return pg


@callback(
    Output("count_per_year", "figure"),
    # Output("intermediate-value-form", "data"),
    Input("map", "bounds"),
    Input("output-selected-range", "children"),
    Input("mode", "value"),
    # Input("date_range", "value"),
    # Input("multi_select", "value"),
    # Input("multi_select_collection", "value"),
    # Input("text_input_recordedby", "value"),
)
def get_count_per_year(
    bounds,
    date_range,
    mode
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]
    else:
        bounds = bounds

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    # logging.info(f"date_range: {date_range}")

    with open("count_per_year.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            mode=mode,
        )
    result = client.query(sql_query, parameters=parameters)

    column_names = ["year", "cnt"]

    df = pd.DataFrame(result.result_rows, columns=column_names)

    fig = go.Figure(
        go.Scatter(
            x=df["year"],
            y=df["cnt"],
            # mode='markers",
            mode="lines+markers",
            # marker_color="blue"
        )
    )

    # logging.info(df)

    # fig.layout.template = 'plotly_dark'

    # Add range selector and range slider to the x-axis
    fig.update_layout(
        dragmode="zoom",  # Enable selection mode
        uirevision=True,
        # selectdirection="h",
        xaxis=dict(
            rangeselector=dict(
                buttons=list(
                    [
                        dict(count=10, label="10y", step="year", stepmode="backward"),
                        dict(count=50, label="50y", step="year", stepmode="backward"),
                        dict(count=100, label="100y", step="year", stepmode="backward"),
                        # dict(count=1, label="YTD", step="year", stepmode="todate"),
                        # dict(count=1, label="1y", step="year", stepmode="backward"),
                        # dict(step="all")
                    ]
                )
            ),
            # tickmode='array',
            # tickvals=df['year'],
            # ticktext=df['year'],
            # rangeslider=dict(visible=True),
            # range=date_range or [1500, 2000],
            type="date",
            tickformat="%Y",
        ),
        yaxis=dict(
            fixedrange=True, autorange=True  # Prevent vertical zooming and panning
        ),
        # title="Bar Chart with Date Range Selector",
    )

    return fig


# Callback to capture and display the x-axis range
@callback(
    Output("output-selected-range", "children"),
    Input("count_per_year", "relayoutData"),
    # State('range-store', 'data')
)
def display_selected_range(relayoutData):
    # Check if x-axis range is defined in relayoutData
    start_date = None
    end_date = None

    # logging.info(relayoutData)

    if relayoutData:
        # logging.info(f"relayoutData: {relayoutData}")
        # Handle both formats of x-axis range data
        if "xaxis.range" in relayoutData:
            start_date, end_date = relayoutData["xaxis.range"]
            sd = plotly_to_datetime(start_date)
            ed = plotly_to_datetime(end_date)

            return sd, ed
        elif "xaxis.range[0]" in relayoutData and "xaxis.range[1]" in relayoutData:
            start_date = relayoutData["xaxis.range[0]"]
            end_date = relayoutData["xaxis.range[1]"]
            sd = plotly_to_datetime(start_date)
            ed = plotly_to_datetime(end_date)

            # logging.info(f"Start: {start_date}, {sd}")
            # logging.info(f"End: {end_date}, {ed}")
            return sd, ed

        else:
            raise PreventUpdate

        # Filter data if start_date and end_date are available
        # if start_date and end_date:
        #     # return f"Selected Range: {start_date} to {end_date}"
        #     return sd, ed

    # return "Select a range to see data."
    return 1975, 2024
    # return stored_range.get('xaxis.range', [1500, 2000])


# Table top recordedby
@callback(
    Output("top_recordedby", "children"),
    # Output("intermediate-value-form", "data"),
    Input("map", "bounds"),
    Input("output-selected-range", "children"),
    Input("mode", "value"),
    # Input("date_range", "value"),
    # Input("multi_select", "value"),
    # Input("multi_select_collection", "value"),
    # Input("text_input_recordedby", "value"),
)
def get_count_per_year(bounds, date_range, mode):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]
    else:
        bounds = bounds

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    # logging.info(f"date_range: {date_range}")

    with open("topk.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render(
            mode=mode,
        )

    result = client.query(sql_query, parameters=parameters)

    column_names = ["name", "cnt", "error"]

    df = pd.DataFrame(result.result_rows, columns=column_names)

    df["count"] = df["cnt"] - df["error"]

    df = df[["name", "count"]]

    df = df.sort_values(by="count", ascending=False)

    columns, values = df.columns, df.values
    head = dmc.TableThead(dmc.TableTr([dmc.TableTh(col) for col in columns]))
    rows = [dmc.TableTr([dmc.TableTd(cell) for cell in row]) for row in values]
    dmc_table = dmc.Table(
        [head, dmc.TableTbody(rows)],
        striped=True,
        highlightOnHover=True,
        withTableBorder=False,
        withColumnBorders=False,
        captionSide="top",
    )

    return dmc_table


# # Table top datasets
# @callback(
#     Output("top_dataset", "children"),
#     # Output("intermediate-value-form", "data"),
#     Input("map", "bounds"),
#     Input("output-selected-range", "children"),
#     Input("mode", "value"),
#     # Input("date_range", "value"),
#     # Input("multi_select", "value"),
#     # Input("multi_select_collection", "value"),
#     # Input("text_input_recordedby", "value"),
# )
# def get_count_per_year(
#     bounds, date_range, mode
# ):
#
#     if bounds is None:
#         bounds = [[-90, -180], [90, 180]]
#     else:
#         bounds = bounds
#
#     parameters = (
#         bounds[0][0],
#         bounds[1][0],
#         bounds[0][1],
#         bounds[1][1],
#         date_range[0],
#         date_range[1],
#     )
#
#     # logging.info(f"date_range: {date_range}")
#
#     with open("top_dataset.sql", "r") as file:
#         template = Template(file.read())
#         sql_query = template.render(
#             mode=mode,
#         )
#
#     result = client.query(sql_query, parameters=parameters)
#
#     column_names = ["name", "cnt", "error"]
#
#     df = pd.DataFrame(result.result_rows, columns=column_names)
#
#     df['count'] = df['cnt'] - df['error']
#
#     df = df[['name', 'count']]
#
#     df = df.sort_values(by='count', ascending=False)
#
#     columns, values = df.columns, df.values
#     head = dmc.TableThead(
#         dmc.TableTr(
#             [dmc.TableTh(col) for col in columns]
#         )
#     )
#     rows = [dmc.TableTr([dmc.TableTd(cell) for cell in row]) for row in values]
#     dmc_table = dmc.Table(
#         [head, dmc.TableTbody(rows)],
#         striped=True,
#         highlightOnHover=True,
#         withTableBorder=False,
#         withColumnBorders=False,
#         captionSide="top"
#     )
#     return dmc_table


# Table, get complete records from gbif_source
@callback(
    Output("datatable", "children"),
    # Output("intermediate-value-form", "data"),
    Input("map", "bounds"),
    Input("output-selected-range", "children"),
    # Input("date_range", "value"),
    # Input("multi_select", "value"),
    # Input("multi_select_collection", "value"),
    # Input("text_input_recordedby", "value"),
)
def get_count_per_year(
    bounds,
    date_range,
    # bounds
):

    if bounds is None:
        bounds = [[-90, -180], [90, 180]]
    else:
        bounds = bounds

    parameters = (
        bounds[0][0],
        bounds[1][0],
        bounds[0][1],
        bounds[1][1],
        date_range[0],
        date_range[1],
    )

    # logging.info(f"date_range: {date_range}")

    with open("get_records.sql", "r") as file:
        template = Template(file.read())
        sql_query = template.render()
    result = client.query(sql_query, parameters=parameters)

    column_names = [
        "year",
        "gbifid",
        "catalognumber",
        "species",
        "class",
        "recordedby",
        "occurenceid",
        "collectioncode",
        "locality",
        "mediatype",
    ]

    df = pd.DataFrame(result.result_rows, columns=column_names)

    columns, values = df.columns, df.values
    head = dmc.TableThead(dmc.TableTr([dmc.TableTh(col) for col in columns]))
    rows = [dmc.TableTr([dmc.TableTd(cell) for cell in row]) for row in values]
    dmc_table = dmc.Table(
        [head, dmc.TableTbody(rows)],
        striped=True,
        highlightOnHover=True,
        withTableBorder=False,
        withColumnBorders=False,
        captionSide="top",
        style={
            "width": "100%",
            "table-layout": "fixed",
            # "font-size": "0.7vw",
            "word-wrap": "break-word",
        },
    )

    return dmc_table


@callback(
    Output("selected_date_range", "children"),
    Input("output-selected-range", "children"),
)
def print_data_range(date_range):

    start_date, end_date = date_range[0], date_range[1]
    date_range_str = f"Selected date range: {start_date} to {end_date}"
    return date_range_str


# Click on cell
# @callback(
#     Output("card", "children"),
#     Input("poly_id", "n_clicks"),
# )
# def update_output(n_clicks):
#     if n_clicks is None:
#         raise PreventUpdate
#     else:
#         logging.info(n_clicks)
#         return "Elephants are the only animal that can't jump"
