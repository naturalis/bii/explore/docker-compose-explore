SELECT
  h3_2,
  normalize_polygon(h3ToGeoBoundary(assumeNotNull(h3_2))) AS boundary,
  --countrycode,
  --countMerge(cnt) AS cnt
  --uniqMerge(scie_uniq) AS scie_uniq
  --topKMerge(10)(scie_top) AS scie_top,
  --approx_top_kMerge(10)(scie_top_app) AS scie_top_app,
  --topKMerge(10)(rec_top) AS rec_top
  --approx_top_kMerge(10)(rec_top_app) AS rec_top_app

{% if mode == 'recordedby' %}
    uniqMerge(rec_uniq) AS rec_uniq
{% elif mode == 'scientificname' %}
    uniqMerge(scie_uniq) AS scie_uniq
{% endif %}

FROM db1.agg_gbif
WHERE 1 = 1
AND h3ToGeo(assumeNotNull(h3_2)).2 BETWEEN %s AND %s
AND h3ToGeo(assumeNotNull(h3_2)).1 BETWEEN %s AND %s
AND year BETWEEN %s AND %s
GROUP BY 1, 2
ORDER BY 2
