WITH a as (
SELECT
{% if mode == 'recordedby' %}
    approx_top_kMerge(1000)(datasetkey_top_app) AS datasetkey_top_app
{% elif mode == 'scientificname' %}
    approx_top_kMerge(1000)(scie_top_app) AS scie_top_app
{% endif %}
FROM db1.agg_gbif
WHERE 1 = 1
AND h3ToGeo(assumeNotNull(h3_2)).2 BETWEEN %s AND %s
AND h3ToGeo(assumeNotNull(h3_2)).1 BETWEEN %s AND %s
AND year BETWEEN %s AND %s
LIMIT 100), b AS 
{% if mode == 'recordedby' %}
  (SELECT untuple(arrayJoin(datasetkey_top_app)) AS dataset FROM a)
  SELECT title, dataset.count, dataset.error
  FROM b LEFT JOIN db1.gbif_datasets dk ON dataset.item = dk.dataset_key
{% elif mode == 'scientificname' %}
  (SELECT untuple(arrayJoin(scie_top_app)) AS dataset FROM a ORDER BY 2 DESC)
  SELECT * 
  FROM b
{% endif %}
