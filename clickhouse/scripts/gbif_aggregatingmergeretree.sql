--
SET max_memory_usage = 60000000000; --60GB

DROP TABLE db1.gbif_enriched_source;
DROP TABLE db1.agg_gbif;
DROP TABLE db1.agg_gbif_mv;


-- Source table
CREATE TABLE db1.gbif_enriched_source
(
    `year` Int32,
    `h3_2` UInt64,
    `latitude` Float64,
    `longitude` Float64,
    `countrycode` String,
    `scientificname` String,
    `class` String,
    `recordedby_string` String,
    `datasetkey` String
)
ENGINE = MergeTree
ORDER BY tuple();


-- Create target table
CREATE TABLE db1.agg_gbif
(
    `year` Int32,
    `h3_2` UInt64,
    
    `latitude` Float64,
    `longitude` Float64,
        
    `countrycode` String, 
    `cnt` AggregateFunction(count, String),
    
    `scie_uniq` AggregateFunction(uniq, String),
    `scie_top_app` AggregateFunction(approx_top_k(1000), String),
    
    `class_uniq` AggregateFunction(uniq, String),
    `class_top_app` AggregateFunction(approx_top_k(1000), String),

    `rec_uniq` AggregateFunction(uniq, String),    
    `rec_top_app` AggregateFunction(approx_top_k(1000), String),
    --`rec_top` AggregateFunction(topK(1000), String),
    
    `datasetkey_top_app` AggregateFunction(approx_top_k(1000), String)
)
ENGINE = AggregatingMergeTree
ORDER BY (year, countrycode, h3_2);


-- Create mat view
CREATE MATERIALIZED VIEW db1.agg_gbif_mv TO db1.agg_gbif
AS SELECT
    year,
    h3_2,
    
    countrycode,
    countState(*) AS cnt,
    
    uniqState(scientificname) AS scie_uniq,
    approx_top_kState(1000)(scientificname) AS scie_top_app,
    
    uniqState(class) AS class_uniq,
    approx_top_kState(1000)(class) AS class_top_app,    

    uniqState(recordedby_string) AS rec_uniq,    
    approx_top_kState(1000)(recordedby_string) AS rec_top_app,
    --topKState(1000)(recordedby_string) AS rec_top,
    
    approx_top_kState(1000)(datasetkey) AS datasetkey_top_app
FROM db1.gbif_enriched_source
GROUP BY 1,2,3;


-- Insert data from db1.gbif_raw:
INSERT INTO db1.gbif_enriched_source SELECT 
year,
geoToH3(decimallongitude, decimallatitude, 2) AS h3_2,
h3ToGeo(assumeNotNull(geoToH3(decimallongitude, decimallatitude, 2))).2 AS latitude,
h3ToGeo(assumeNotNull(geoToH3(decimallongitude, decimallatitude, 2))).1 AS longitude,
countrycode,
scientificname,
class,
arrayStringConcat(recordedby.array_element) AS recordedby_string,
datasetkey
FROM db1.gbif_raw
WHERE 1 = 1
--AND year BETWEEN 2001 AND 2010
AND decimallongitude IS NOT NULL
AND decimallatitude IS NOT NULL
--AND countrycode IN ('AU')
AND notEmpty(recordedby.array_element)
AND scientificname IS NOT NULL;

--1400 - 1900 v
--1901 - 1950 v
--1951 - 2000 v
--2001 - 2010 v
--2011 - 2015 v
--2016 - 2020 v
--2021 - 2025 v

OPTIMIZE TABLE db1.agg_gbif;
SELECT count(*) FROM db1.agg_gbif;

-- Select data uniq values per cell
SELECT
  --year, 
  --h3_2,
  --countrycode,
  --uniqMerge(h3_2_uniq) AS h3_2_uniq,
  --countMerge(cnt) AS cnt
  --uniqMerge(h3_5_uniq) AS h3_5_uniq,
  --uniqMerge(h3_7_uniq) AS h3_7_uniq,
  --uniqMerge(h3_9_uniq) AS h3_9_uniq
  --uniqHLL12Merge(scie_uniq_hll) AS scie_uniq_hll,
  --topKMerge(10)(scie_top) AS scie_top,
  --approx_top_kMerge(10)(scie_top_app) AS scie_top_app,
  --topKMerge(10)(rec_top) AS rec_top
  approx_top_kMerge(10)(rec_top_app) AS rec_top_app
FROM db1.agg_gbif
--WHERE year BETWEEN 1900 AND 2025
--GROUP BY 1
LIMIT 100 format Vertical;


CREATE TABLE db1.gbif_enriched
(
    `gbifid` Nullable(String),
    `datasetkey` Nullable(String),
    `occurrenceid` Nullable(String),
    `kingdom` Nullable(String),
    `phylum` Nullable(String),
    `class` Nullable(String),
    `order` Nullable(String),
    `family` Nullable(String),
    `genus` Nullable(String),
    `species` Nullable(String),
    `infraspecificepithet` Nullable(String),
    `taxonrank` Nullable(String),
    `scientificname` Nullable(String),
    `verbatimscientificname` Nullable(String),
    `verbatimscientificnameauthorship` Nullable(String),
    `countrycode` Nullable(String),
    `locality` Nullable(String),
    `stateprovince` Nullable(String),
    `occurrencestatus` Nullable(String),
    `individualcount` Nullable(Int32),
    `publishingorgkey` Nullable(String),
    `decimallatitude` Float64,
    `decimallongitude` Float64,
    `coordinateuncertaintyinmeters` Nullable(Float64),
    `coordinateprecision` Nullable(Float64),
    `elevation` Nullable(Float64),
    `elevationaccuracy` Nullable(Float64),
    `depth` Nullable(Float64),
    `depthaccuracy` Nullable(Float64),
    `eventdate` Nullable(DateTime64(3)),
    `day` Nullable(Int32),
    `month` Nullable(Int32),
    `year` Int32,
    `taxonkey` Nullable(Int32),
    `specieskey` Nullable(Int32),
    `basisofrecord` Nullable(String),
    `institutioncode` Nullable(String),
    `collectioncode` Nullable(String),
    `catalognumber` Nullable(String),
    `recordnumber` Nullable(String),
    `identifiedby` Array(Tuple(array_element Nullable(String))),
    `dateidentified` Nullable(DateTime64(3)),
    `license` Nullable(String),
    `rightsholder` Nullable(String),
    `recordedby` Array(Tuple(array_element Nullable(String))),
    `typestatus` Array(Tuple(array_element Nullable(String))),
    `establishmentmeans` Nullable(String),
    `lastinterpreted` Nullable(DateTime64(3)),
    `mediatype` Array(Tuple(array_element Nullable(String))),
    `issue` Array(Tuple(array_element Nullable(String))),
    `h3_2` UInt64,
    `recordedby_string` Nullable(String),
    `latitude` Float64,
    `longitude` Float64
)
ENGINE = MergeTree
ORDER BY (year, latitude, longitude)
SETTINGS allow_nullable_key = 1;


INSERT INTO db1.gbif_enriched SELECT
    *,
    geoToH3(decimallongitude, decimallatitude, 2) AS h3_2,
    arrayStringConcat(recordedby.array_element),
    h3ToGeo(assumeNotNull(geoToH3(decimallongitude, decimallatitude, 2))).2 AS latitude,
    h3ToGeo(assumeNotNull(geoToH3(decimallongitude, decimallatitude, 2))).1 AS longitude
FROM db1.gbif_raw
WHERE year IS NOT NULL
AND decimallongitude IS NOT NULL
AND decimallatitude IS NOT NULL
--AND countrycode IN ('AU')
AND notEmpty(recordedby.array_element)
AND scientificname IS NOT NULL;



