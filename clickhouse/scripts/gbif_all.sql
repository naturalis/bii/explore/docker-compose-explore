--
CREATE DATABASE IF NOT EXISTS db1;

CREATE TABLE db1.gbif_raw
ENGINE = MergeTree
ORDER BY tuple() AS
SELECT *
FROM s3('https://gbif-open-data-eu-central-1.s3.eu-central-1.amazonaws.com/occurrence/2024-11-01/occurrence.parquet/*', Parquet);



-- To expose data in Postgres
CREATE TABLE db1.gbif_psql_raw
(
    `gbifid` Nullable(String),
    `datasetkey` Nullable(String),
    `occurrenceid` Nullable(String),
    `kingdom` Nullable(String),
    `phylum` Nullable(String),
    `class` Nullable(String),
    `order` Nullable(String),
    `family` Nullable(String),
    `genus` Nullable(String),
    `species` Nullable(String),
    `infraspecificepithet` Nullable(String),
    `taxonrank` Nullable(String),
    `scientificname` Nullable(String),
    `verbatimscientificname` Nullable(String),
    `verbatimscientificnameauthorship` Nullable(String),
    `countrycode` Nullable(String),
    `locality` Nullable(String),
    `stateprovince` Nullable(String),
    `occurrencestatus` Nullable(String),
    `individualcount` Nullable(Int32),
    `publishingorgkey` Nullable(String),
    `decimallatitude` Nullable(Float64),
    `decimallongitude` Nullable(Float64),
    `coordinateuncertaintyinmeters` Nullable(Float64),
    `coordinateprecision` Nullable(Float64),
    `elevation` Nullable(Float64),
    `elevationaccuracy` Nullable(Float64),
    `depth` Nullable(Float64),
    `depthaccuracy` Nullable(Float64),
    `eventdate` Nullable(DateTime64(3)),
    `day` Nullable(Int32),
    `month` Nullable(Int32),
    `year` Nullable(Int32),
    `taxonkey` Nullable(Int32),
    `specieskey` Nullable(Int32),
    `basisofrecord` Nullable(String),
    `institutioncode` Nullable(String),
    `collectioncode` Nullable(String),
    `catalognumber` Nullable(String),
    `recordnumber` Nullable(String),
    `identifiedby` Nullable(String),
    `dateidentified` Nullable(DateTime64(3)),
    `license` Nullable(String),
    `rightsholder` Nullable(String),
    `recordedby` Nullable(String),
    `typestatus` Nullable(String),
    `establishmentmeans` Nullable(String),
    `lastinterpreted` Nullable(DateTime64(3)),
    `mediatype` Nullable(String),
    `issue` Nullable(String)
)
ENGINE = PostgreSQL(postgres, table = 'gbif_psql_raw');

-- Insert data from Clichouse table to Postgres table, 60k rows p/s
INSERT INTO db1.gbif_psql_raw SELECT *
FROM db1.gbif_raw;


-- Create enriched table
CREATE TABLE db1.gbif_enriched
(
    `gbifid` Nullable(String),
    `datasetkey` Nullable(String),
    `occurrenceid` Nullable(String),
    `kingdom` Nullable(String),
    `phylum` Nullable(String),
    `class` Nullable(String),
    `order` Nullable(String),
    `family` Nullable(String),
    `genus` Nullable(String),
    `species` Nullable(String),
    `infraspecificepithet` Nullable(String),
    `taxonrank` Nullable(String),
    `scientificname` Nullable(String),
    `verbatimscientificname` Nullable(String),
    `verbatimscientificnameauthorship` Nullable(String),
    `countrycode` Nullable(String),
    `locality` Nullable(String),
    `stateprovince` Nullable(String),
    `occurrencestatus` Nullable(String),
    `individualcount` Nullable(Int32),
    `publishingorgkey` Nullable(String),
    `decimallatitude` Float64,
    `decimallongitude` Float64,
    `coordinateuncertaintyinmeters` Nullable(Float64),
    `coordinateprecision` Nullable(Float64),
    `elevation` Nullable(Float64),
    `elevationaccuracy` Nullable(Float64),
    `depth` Nullable(Float64),
    `depthaccuracy` Nullable(Float64),
    `eventdate` Nullable(DateTime64(3)),
    `day` Nullable(Int32),
    `month` Nullable(Int32),
    `year` Int32,
    `taxonkey` Nullable(Int32),
    `specieskey` Nullable(Int32),
    `basisofrecord` Nullable(String),
    `institutioncode` Nullable(String),
    `collectioncode` Nullable(String),
    `catalognumber` Nullable(String),
    `recordnumber` Nullable(String),
    `identifiedby` Array(Tuple(array_element Nullable(String))),
    `dateidentified` Nullable(DateTime64(3)),
    `license` Nullable(String),
    `rightsholder` Nullable(String),
    `recordedby` Array(Tuple(array_element Nullable(String))),
    `typestatus` Array(Tuple(array_element Nullable(String))),
    `establishmentmeans` Nullable(String),
    `lastinterpreted` Nullable(DateTime64(3)),
    `mediatype` Array(Tuple(array_element Nullable(String))),
    `issue` Array(Tuple(array_element Nullable(String))),
    `h3_1` UInt64,
    `h3_3` UInt64,
    `h3_5` UInt64,
    `h3_7` UInt64,
    `h3_9` UInt64,
    --`h3_11` UInt64,
    --`h3_13` UInt64,
    --`h3_15` UInt64,
    `recordedby_string` Nullable(String),
    `latitude` Float64,
    `longitude` Float64
)
ENGINE = MergeTree
--ORDER BY (year, decimallatitude, decimallongitude, class, collectioncode)
ORDER BY (year, latitude, longitude, class, collectioncode, h3_1, h3_3, h3_5, h3_7, h3_9)
SETTINGS allow_nullable_key = 1;

INSERT INTO db1.gbif_enriched SELECT
    *,
    geoToH3(decimallongitude, decimallatitude, 1) AS h3_1,
    geoToH3(decimallongitude, decimallatitude, 3) AS h3_3,
    geoToH3(decimallongitude, decimallatitude, 5) AS h3_5,
    geoToH3(decimallongitude, decimallatitude, 7) AS h3_7,
    geoToH3(decimallongitude, decimallatitude, 9) AS h3_9,
    --geoToH3(decimallongitude, decimallatitude, 11) AS h3_11,
    --geoToH3(decimallongitude, decimallatitude, 13) AS h3_13,
    --geoToH3(decimallongitude, decimallatitude, 15) AS h3_15,
    arrayStringConcat(recordedby.array_element),
    h3ToGeo(assumeNotNull(geoToH3(decimallongitude, decimallatitude, 9))).2 AS latitude,
    h3ToGeo(assumeNotNull(geoToH3(decimallongitude, decimallatitude, 9))).1 AS longitude
FROM db1.gbif_raw;


ALTER TABLE db1.gbif_enriched
UPDATE decimallongitude = 0
WHERE decimallongitude IS NULL;

ALTER TABLE db1.gbif_enriched
UPDATE decimallatitude = 0
WHERE decimallatitude IS NULL;

ALTER TABLE db1.gbif_enriched
UPDATE year = 1500 WHERE year IS NULL;


CREATE FUNCTION normalize_polygon AS (polygon) -> if((arrayExists(point -> ((point.2) < -90), polygon) AND arrayExists(point -> ((point.2) >= 90), polygon)), arrayMap(point -> (point.1, if((point.2) < 0, (point.2) + 360, point.2)), polygon), polygon);

--ALTER TABLE db1.gbif_enriched
--UPDATE "recordedby.array_element" = ['null']
--WHERE empty(recordedby.array_element);
--
