SELECT (topn(topn_union_agg(topn_recordedby), 100)).*
FROM gbif_aggregated
WHERE 1 = 1 
AND ST_Y(h3_cell_to_lat_lng(h3_2)::geometry) BETWEEN %s AND %s 
AND ST_X(h3_cell_to_lat_lng(h3_2)::geometry) BETWEEN %s AND %s
AND year BETWEEN %s AND %s
