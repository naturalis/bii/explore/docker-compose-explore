WITH a AS (
SELECT 
    year,
    just_count,
    ST_Y(h3_cell_to_lat_lng(h3_2)::geometry) AS latitude,
    ST_X(h3_cell_to_lat_lng(h3_2)::geometry) AS longitude
FROM gbif_aggregated
)
SELECT year,
  SUM(just_count) AS cnt 
FROM a 
WHERE latitude BETWEEN %s AND %s 
AND longitude BETWEEN %s AND %s
AND year BETWEEN %s AND %s
GROUP BY 1;
