--WITH a AS (
SELECT 
    h3_2,
    -- ST_Y(h3_cell_to_lat_lng(h3_2)::geometry) AS latitude,
    -- ST_X(h3_cell_to_lat_lng(h3_2)::geometry) AS longitude,
    -- round(hll_cardinality(hll_union_agg(hll_gbifid))) AS cnt
    sum(just_count)::integer AS cnt
FROM gbif_aggregated
WHERE 1 = 1 
AND ST_Y(h3_cell_to_lat_lng(h3_2)::geometry) BETWEEN %s AND %s 
AND ST_X(h3_cell_to_lat_lng(h3_2)::geometry) BETWEEN %s AND %s
AND year BETWEEN %s AND %s
GROUP BY 1 ORDER BY 2
