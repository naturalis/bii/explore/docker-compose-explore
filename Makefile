.DEFAULT_TARGET: all

.PHONY: up
up:
	docker compose up -d postgres clickhouse

.PHONY: setup_postgres_10
setup_postgres_10:
	docker compose exec postgres bash -c '/bin/chmod +x /var/lib/postgresql/scripts/*.sh'
	docker compose exec postgres bash -c '/bin/su -c /var/lib/postgresql/scripts/10-gbif.sh - postgres'

.PHONY: setup_clickhouse_sample
setup_clickhouse:
	docker compose exec clickhouse bash -c '/bin/chmod +x /var/lib/clickhouse/user_files/*.sh'
	docker compose exec clickhouse bash '/var/lib/clickhouse/user_files/setup_sample.sh'

.PHONY: setup_postgres_20
setup_postgres_20:
	docker compose exec postgres bash -c '/bin/chmod +x /var/lib/postgresql/scripts/*.sh'
	docker compose exec postgres bash -c '/bin/su -c /var/lib/postgresql/scripts/20-gbif.sh - postgres'

.PHONY: all
all: setup_postgres_10 setup_clickhouse setup_postgres_20

